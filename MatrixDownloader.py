import dataclasses
import enum
import logging
import json
import sys
import os
import base64
import http.client
import threading
import hashlib
import zlib
import urllib.request
from urllib import request
from io import BufferedReader
import argparse
import datetime
from concurrent.futures import ThreadPoolExecutor

NO_PYCRYPTODOME = True
try:
    from Crypto.Util import Counter
    from Crypto.Cipher import AES
    NO_PYCRYPTODOME = False
except ModuleNotFoundError:
    print("Missing PyCryptodome. Install it to get 300x faster decryption times by running: pip3 install pycryptodome")
    with open("mini_pyaes.py", "wt") as f:
        # https://github.com/ricmoo/pyaes
        # noinspection PyPep8
        b85 = b'c$~GJU2`4Rm7cGqUxB%pkQ^P$ySw*SlY*;kj;1Q5nz(AL$qkD}K_Dc<q5u{Ejjc)Y-}6522G3rUAbDmgNg@jZ&gtI0_xfJX>YV-4>644AcYnIPy}$hH{mEBvZ!W$)`R@BW{djq@)RV7%xWBx6^6}Z{r%&F!J-@pC<xl<N>67c5`;)8h-o3s2?ouCqqhG!}ee##vo9k~LzV`O=`kVVd>G!`oeR6eub$@mK_Uhly@2_sIPyX%l;{N9Lu?zq9^ncu3Uw*6$-`w7xT-?0-;c$0<`~AiJ$7g5fZ{MDsoxXbV+t2^-<iP*_^Ua&f-+%r4cbB)m?8`6z<?-MD?&agZ|EJ$Q`uopcKK}dP$Ist|Kj^nFZ@$+q+&+2z@te!9PtGoGzI%6maewxgo7*=}`JZRcZ!hn^zr8-;KVGHRpMAFG|Lva_{k+ui^Qxcw_;Gu<lAiwR`kS-+n=^0GQ_kghvA0)u_fMa4kBettYeO$iuC7n~iD%>6{tjOrmwEct)%E%956AC6dGhI>Z?3MNKE3$U?bEM(iLbcC!^fVzf0*lw^ZPU3?ELW6@!RL0eD(4D?GMlYdV7BT&E?0Jf4#VTcYpG^|D|1d{_#iSFQ?=0AN|7feEiWzS6`o8-CbSZ-Jf4yT>94sf8uyMex=Wi|9bVvL%)8w#ou`7ao>kuK78dD9&vp7*~b@e&+qO|boyHC_4nU>b$NSs^Yz)S&i&2Z>Hk{l^CcbD{Crst+xmRT|L4W6E_(WEIjo0#=!d21_lKGf%i0fXI~4tt^pk$SwB^v+VbQO0JM8*H?}vRqEW5r|mP6U}(W;NG`lx=rmcv?fvyG4Fd%3)R@rTn_d}Gy@7yi9zE&6!UpO?iS%et6;u^qDhrdzE`@tv1Nt4R9nrc0*fc=^5`^w%{feQFywuPi{{Dq1s7vgn^(zw2C4i`443&XZMc-*mZrD11*>TzCD7yKR2vtp8jNe1AV|y5z2p=)ZK|;*HR{``D^gPp(h!uuZ>Ov_o2}zN<&5?Bt@2-?hrBHLr)lg|jzGw_EjhEo9Nwv&gg`*tf<}tgKAizAs~UwKIz@Q`({F_Vv)&7M`Jxoo@Q34x={CN<S>ycBnj*{?SU?p{IkEuxJ5G?T4;UXdBq3%+_ejbZPC}nu`y3(PCFUq%NcVSPL7#-f#-~gs!OzZ}qSj3qfTW`W;)Z2hyWg7OqEU^R)@(z~8jZdh&HmhfUI?i)fn`U4_+e+*G@!AJ^4#rVZjQ+Ico!e@+LTwIo%yWdTTu?$Y$>j-2TqKF-PLt2%^L3((3JZS2B!>vTCJ?F+))d0Jg#L)MWjcHGL)S*q5k)6r-6hJK+9MC4ZvXGK)B5NQV6#gX$50w66y=6L$`prs*e`gT4@$R&jx(b0FfDSM-@6~u`BMW0xMzN-J^-`*mAEeX@@C1~r0py>R|A+u8)9@52$BT2d{&#uGPO(o!!^O08ZFI`$+M1dtZtF-d!^p%czf6(6zB}DP{9dt++LOvuWEOD`lY*tJp4P{1tOS5jWM0ASn)-rhxEnc!9$v`YP)3Nr=OYp)w2kXt%S?XJLu^6=KlhoPPYGp_Nba0*H@S|#Ld3tTpRv#3vTUE68Z5s!!6G5Zc741H|>w`#XY<|k#ZI)%@k&!7(!oCf0=<CpnG)`4V*^><k8p;`>9+pAGmvcq(F(PHCG{XJffTnRwXe}a}h5?dj={Lo4k4cbVR;#qW$)2*^dQ_hPYRu9e1a8AzKFFNxKzUGr#(`snwauuRg}W>^x5Hl4VKjLbZh=&=!+JbRbhhGPQ!$ww7-meTfdxSHhmo`3p^uC0raoiSX{%<dzde|C*)=QYuu$1XY;87g(lU;#47Jk5P+RuR=6|>G#ucE77#+UJ#w<hsBv`g{6>BUzjl{HJm@Do9jl@C^BP9_;Co#yF#bOn0SRZr)`)~cnJlZuZ_9Hf}H4PiI+YL4wE}O|#ZDFwgs1)W^i|d;;PMfgWn`N6r+mvNk|7|F|wLIZUB}~{^47Hr)m&@s^ZCiUzIz4$3J?Wm>j##9Wda6CK&s$l0s}hO4?pb@?Bz9Zb+g39YxV24URT8I@bE&0eTQZ$YOI_Q%ZB@dh4cT&CyMDvb@yWXTQhnD`_ry-7lDZb|?MhqP>x#bawdtvf&8V!wCM8xhu=bj3M;JD(Dd{n^Zrw<Kz%b~cxNT+AcikZ+)I(a|(pK4#EP?Ay_p$4gxXAEmvHDo5+>!IhdC!T%&{<038g6}Wxz;V&9&WmHlGbc`s+LM+iF$D9eziAmS^rNf(I)NEjy+)v(zc~83F5q3<4cHAmo)AxrzgejsbyqLk6UZ)1@E9=OT>1(Xv<xev7ui2O_zzmi4<APq-~YlNP)MmuV!wn11*_&KzlDWv_-7BZM$|sqLylsjliUAS;xTf>*8zQTW5(9z9ylt?rFb^&R7yI+0aEL|NEu~E<IaMw2f?i+k4e6B(}S32njM<bkn5A%T*g!H+D@AFH41zZMkGgUdGC3k@{`7AI&=PrfW$EwoMYVX`P$SxXXY_AorqsqbZV^T}$3Ep4vKnpb-selWUUXvP%dVamM)e=Ad$Gva=O4!8u669LjY`w@y}HOI^~IMzAIUWm6E%)};yvj=sOveJ_~s(v$XIIw8~2vW`dkw(?9(`>v0oI9n-_F`boeTlSK1DyeDv+R9n!R8wkogp3PY%(?26Btw;5Ds^jFTY>RxU9zwx#}cWOZO2mV@}g1z?0N3`L~WS7Qi>Lhfm*fJqLtw>bdr15ZV_G-dAjWQa+6!wO6Q5BxLTSvUc0}qO~-~&mxAef*hy_<mj-9thW=W085s=i%+8M(HEF=ksp$eTuuYGTtCnBL1o_&!rd^kGNE3I7f-RsFP$7a0f!vV{bth(!8t9=Vo_a1lhqQRZ50)lPD+rB5Un-jQ=t!g<k8?^@#z-3}7gM*rwOZ{N*M`hW5ZPE63mkQA+r~}tgF1BSH{!SJ7kbua7qsUpbtVmws7ZXK)RLH{YsjtXS#&}h(ZQxQ4^<&<kOcNkcaW{fSvG>F(!u17h(UH$H<Nbh1hi}DZ;?mYWfySAa@$oxgOoJZTnICyt1?A3VW9HXu$T&7bas+m9UO73eznOem-m*p5FKJk*h``!eL`jev!%0H%kH&E1tfD=4@s>Sjx(&1R~;c|&9h61@uIqK>aAh5q-sf8D1qE}HhxDIbW~DA$L&^sgh|?)W%&yZRn|=ho}5JtS<tOI=0UoKrc~K}S)ld3-a3AjK2)SYzw8oy?d;L!tlg2FmMW!8M~8zOFM1kD!E%%<tgcM3?#HgkILQ8WG(#)WViPV>{!G%6vV)Bx2eq@A606o%2Y~2>A}>W_`fRT<9@0(OZ*76BLdJ7SrP*OrL;|unLXrnq5FuTE*Qzu7t{vB%3dTa)D_<dD#scZV_^~Pg(ppeQ?Pf2!u^v$Jxl4u`VbU%=l^xW%rJY@B+%_ZT*uMjRtQ;w>Jn4X2ZbU$GjdEg9_lPaD)UMSY#g*UcId+K_d9@r-K{bd+<$81lWI#fwn~<&Pw~D|0F#4#(t6;h_8{UVRm0f5O5R^v}V_!gsykbY2!!>ELyt&gx9e(bPinN(~A~ToZZ&GB!?aFE#DoTkmOK{XDRpF7j?anij*ULXv*#MG2skc<szO^PS2nmjypiWZX$1*5%=^Rp%yU<$YadaUGxulD*O#dr)s0Y`sR6-ZYmQ1R?CnI^l<?FG@F)EkUj_SAy4wxWOE7NYT&NDW7H|@48igrattWW|#@TJwdr(~qcj*tzmwc)y@y1KHwi1t@cF9EKkfDcc^>Z{ImtHc{JGRR|h?u0mR*dvUsqTRhrXHS&eB{w*f13qbW>?6x3N5uEFO}ax-42ro?L`Gzz6(&T199d^4jdMn<jg#)9xLUj}!RqzPqDO6t>vhCBdh!m!ke#SY@xDB{LLv@du2B0+A}HmTI4ibtuFeO$Gp@bjw5np)_4Jh%F5x6FB2B;~Z4O@KMkR6x;YM&EwIE28KeQg9-8#91IO_Qni8jJ}3AJP#Q=_d>EGfUdvNk<%^Jz$tC6NRr32h>J&x&X_VnkBeiWDl0m!xmHL$?y>bG;{r<O-vtr6ih)Vzk)Z8I{yjCqjTh*oyiiFESspaS3sekIy(Gov5Tl2T3e1JD!Ln@U%_J;e0{rqI+{r3L~T~xDdq(+TMa=(|=d&7va6E5DG(<sIwt>z{x5U((xe@QiGLE;u#g&BWO5wDO+>UDBajBh(1YtEcu>%q#kP}melbp4cJ^B%E_4knY+$edyY3`f0P!>-QnXFC(<$@3TB=DYx{T1874(B6;7E%yAuhKf9aD7y~^SYsuNlBcbUh`c9MGO>t$mB^3fZK2Hr;Dq-+)CGTCQ&X5tx{5P43vT|uG372N4!OHJy$<4ZT^9@RdDGy`+S$&pwRIbj~j(&Wx$iHT+vQ0qB~y(Dr(8I*YJUmMO$cB^eVS)Na~(gDeg>1gr4I(9{b^7WNNlkPQA4CiEW{SA92>DEFj)dHDe*`Dkgq};Ef5dvdur!0z=Nz#=Goe)Q<law<R7k=cRcCtw_pPgVIJBC1a8Cx<B1>&8wjf5{bq&Tp&gjP};A>hyvDq<)^PGuRno}4K8#sTrtK9Z?Q^px<jrPNO(W!gC^DvHu%q@0k;M{COpNx?=iB{3n~m08pC%gRWewZi1Wagmm6xs6CdMz$yF14<L*yvQf@WC=6GkGiHF^FVaxd<X4vAkc{F6q4v`uHZ<{bqSe&oTsF&5I1se(h2N-k~m@>v1f@87u}F#Yh8q`IsnO8BK1|MrLf!^s=rBZ$$Ru!S!7a)MzDx+_W7=>R5%sa59AxAvn)niECp1QL1?Zpcqfh3i4-1#+M2#4v#f{F^GoVWChp7%4!UbkWSxX{t|%4^yetTtrR+kdl*#Xu&$z&*j4mO*dj6G^fzwd@(F&ypJ-S1MuCP?WmNsM8b;vq(JKbH+tc|8*(@549!XF9t!a7_J*$IcK6X_)6+i=VoUo43$F1sS@#E-a7B1dA)j_Bwoq6ArOSz#%Sq_SDcSaim75n(D-$DfkVD9O=7;pg-zDg!QKE?9WofYOuPJ#y{Zm@EknN;hlT3(`MUBR{urF{!46fKyO{oOTI(C&VMvSzT&-FAmA6T9U$JZKZM2L75qu&4xK<WA$998}XA=c;(@#JG!)xsMsjr)v0THj{b%aPok%YV-%p|TwF_(s7WOpx$4hadRbgh$8l2kT1f5)e2Cz1X*)#%xhv(M!yHMYi9;RH>v3c=`l6Icq1;BApg)q`NZAjavJY7|L9kLTd4KMe^oSC}-N~`^TydP8t0IU*RahoQAEd~uU}i%KSIQq1??H+xD=T@UkVCab`33gTZbCk`k{-!$>DpS0ys<o6!XXUbCa)k%K>odt%It&s5oXIC63bIzBkAwOr7-F{F7e9L$qq``4vHW-f{sc`jI_u?%Ak$UR2?X(NC_~02UM(<-St|uNgG1Eype+umllUp8>KDVpPVuNGH<wD;w?w5nOe~5thG}nm1Zc6lBb~(>d3D;@|56cjdT?B3yGQ3mPlX!2LrCOO>&yBo3<_+m4U4jCT6lT30+w%EqDxxCK0ZJ2-LfccudkjRasYCw-J;mx|R%7S93cWSjm2HH$?SPUnH7pI>)0ON!%3QODrV`(ZA)Ub{7yR8M$Ds&C>!#u~WAsSJxvc_>sW5-mH9t;u|G>8HuvGYA)NY_(}G=kS%S+g;LxJp{7Jd7bo>7)8V^VD~q(a28{wW{4RmL^mas~xTu8S90-W1Jr<{23kOUIRUcfPRBBg?7UlpkQ8|!(CPSB^Z{#*Q9vK+fVwVUINEbU~<vU4<c3ANT#RS~*M)XGTN}aS=L6sTkk`y5nyl$tj7K)m7;^Z%SoSim>E-g<e_kg#SgHB{$SWI0QC9=z_R1n!|$(a;nxHrOa?a@I?C=DM@5TyOxMrerXpudW{kpS2UX$MjrX@its2Q+GgGR<kUT_xm{X|aYIn?r@3V3pIMW7mkr_ubVor695bh1w%lw&F*qO)ReDQ6|_(eqESV>eHOK?F*GfvN1|qx>a&;hK0(*1QgOtsursa<Lb_aHb|kC8*n6GJ9Rf#Fjf*7WVG7~_%TTm$61lolV$FQit*P2)60>-%DiIIl|7Z}=oL+_j`k(@`W6oDK-oqHb9Kg)i5nHZZWuwzb35i_L*5;=N|@wt$W!^UVs)*%V{*HDMcPaqDRt4tS8(6b@zluLm4!)Xq>8R2%Kc&Ps8+3*I7h6_^-g+hl0R3Pr0|7=m`p=Di(sH>C@?99L{PHbkVxsK&RRlC0$W^Z-IT1#cIdJcy>^0d!pw!0EtE3pU`f-G+cYj-EA~Gqdh4Y7vc$Tdo=N68%c3hJp*$}0p!*i*2y)K*M*nOjl+-?8{E*6>wEVD>c<DTG8d8$A+xAhdP$pOCSNW<GhqSgr;NA|la@4fx`AHCoaw*EoGP93F0UOr85VsQA+sP>1&a6a7NDDxBCh#E@q=H|%`|b+hlxR}}uwoKjQybbSY&VCqES-kC_DwgSg|+hh$ywcs_AkgX<sJ(g7tmQ&WXaZ;7_^cJ>5DpHXZ)2yKrasPmBvczObQV5XZ$dwVBM=x2v~5{o4eK$B!wn?6!F|?9V(XXRDz2OKr-`<z*VP2F_2Qf;sixx1g?rneca?K%~et|fsRL%y(Fz_G$z*FRq_QzrGt>T{s^12<+4Rc?uzHtr?B`HzqgXl?v#=ix&`u&3PM*$u7s0??uHJUc7ZNKH9=x8v6A!=^OVgUc5O?iKI+n>JX*IIX*xnVI|mvG-63I?3~Vkm9^CxlA+QYO4AfT?D@rGbUF9dcOp0SyNrdu6w^ccOa*|4cO3uNN&Pu38o<*6`#>dD23Fzk7RS8L=`dl3U$V(--0c}sN?q0aQCH-=7bf-pxfmM{WkcM(J+7+1yn&_J&93?YE5<6#@!7+5S=-YviO(7_W$k<gpj!r$XE7=KFBgu5~zf)SW1$UKb;|$)*1K7bvDBaw>AU$iYCt>vUCAubxffY9vSKAQ!mF}j*d8NE3kJjm{89EuRhTK8RUuCjsKI>5Bd-9=j?A5L6;*Nrh2{nc!%Uy}SNJa}Tnbb#mK{JJ#iuSQ)7w{-{E)H7>Ugab0(%G_&rVkAr8i$0PQf+zs49XyDBAcm$JjejcEKmhqsJK&+Bdk>1R0%B{K`KX~5v_Hu2mu{?IgXSu_O$6~;<<;9Jy$}oyXLArb@@RHP*z&2J2G;I$}osr6Fn}ugG6IQJEujjQ~z`@w7#f>BCxtKZvIe-my`?83Eo%wjj{<YUsFgSDCyJ~90IP?1n3@Zv_`6P{=TyiDp;~&<Y1LBfI_hBjkea&r34Owrmaqu77Alh4GAm_c<c1GxED{#nVNYlW%esQT{0d?n>@XiAg?a<swQN#SS&S7ZVBn?>VVWcb-PtsFL^PvMai-o0>Px?R1Owk`YqIyb#jUbN8e1R@4QdOO9$svY^A)h17?tVkXUJ{mFlEEjA-lP(jL0j(lfj<b!5&$b^<4=N1=A+Uj9beytyJl2Io>KT1bY7@a;r<m0Bw1USb3YLAWU4cY#&jr`h9<vcCO3oyV2R8o8eK7_~aOdse8+>Ns7S#Ne&AFKuy>ZsDZPg8UP7DMs&DO|t1q7neFCCeD->WnbkK*<hLuK>9*SLjHPN+`W;$kcvsV8>t7mfa8cxQRzT6!ETjmz8nu*=+XlogBlSVg_|ID&BU&B5%h6GB$Ar48WU3hL^pvR6tPe4E?dzeljsq!BH=6)BO~ukpiS)B^+oDpI&nIoj$QG12BoORR)U>|!J}1x8|W_R6JALpxtC*CcD^{>>sp?yI&A<tVB4_RohtrLn4vS4?p7mx_<BNj+-@SoT1<E;{V%HnD#8pZiiCI)Y0MR|tFA%W>cBBpn$Nf@nG6bKd?3?NqkE`RlIQHEnza8bp((ClrFXT_tB|9il|-8<QFN1$c8D&`7>NuDu@XQlb|rFcu#uEFIdI3WKnxTfXlv9&6Gx6vTQ92)4W?6-Y7@K4is=9yyHeP*63`3PK9mu=a*ExZHo`|&6g!3DI`AoNCXQ6XEFsqUV`rD<;?bdyabg9?oic^)x_H~jWF5O!`hD7}P8E}oado^)NJRuA`%gPvhE3bS0Xl}%85pzBYTC$nbS5&bN_v#KB6IWr70q?jz)i?T-FP=^gLkV`Kh}f=++jK)SrTbdc0YHcDy?&sW?;R#bVHHY=v3%H@hKFWl+WjCRzm5rHVKT2ypz(AwhK9?@_KO9Ar&mR!e%te<)M}Kx`HgluGqb;npdM4xtr_L@)hs#(IVN<cDE6`$~9-a@xjeVw1C7WQ~Pjxs#Ddnmns{g=7y*&;Km*~cHI&7#j$JhUm%qzZy0LRmeC?J(b%1S8;v8n$NEwdrK8>GLr}zDlq5Kl!yQ!jM|8ZaqIJhQ3IJ<0x+rZR=_4-KaSYU;P!Q?lVg!M9*FGr66(dG<TbrRE!J4Iq*29k2m7<BU1WeD;M(pZLCOdZ3VNjf-#wDw%bO~23MS}=hDUr}isiy8ELg@d9U9o#6SUKlv7EV$^8*L?amC2=MXBLMp)`(pz9TY^844TdXg&%Ej0sT41iBVc0Uq)sKgiTLzgIwIHmC#`t(56JRP+(xF>$DNOE@bT8=Ov453`!mY17}i9a<aqK5xWur7*fhmB^jgVp2w(bBuDp{zwX23V2E9{-D`L3+AuW4ZW!H#Cx%I@7~qkMB({ruaKpR~uWj678?h?~Zu~@dQ-0$fd^4B<_DB0jGOt5w1pfpy2M1Uw8PKNMO>(yn^`}=sZBGZ&X%^F@-;Eh8M|p7Ux;tzjH-)~EF<OV-noIx^C=>*VUE3fUhRu-&fLm&|*eM-d4Khj%t{FH%vUFOCU1629e&d4pthPCJrBzAtK<`G<nk;yPVwom`V%Jn%@gsI6o*c1jB4JF9T?rGUTy(HD0y@J<9p53VWOjqcuX<|Q*p&B(z;wXc-T?q*I*pbgnRgwWK}xEl-KAU^d^oXW1d$$6aqOBc`I#1vRzocZJjeYW3MLW+ba5Xfi9Sp$NbIVu>}6nLL;r|fSNyO`SQ1#d%bnPDqu(#}R%+vZ8nNrbJ{tLV*6#F1AHoxoVQml#3Q|~#0tUR~psQSfbL<MqmXl*wnkpD}O0hsfEQK~*b=6VUPOIzwQ7#wHO4Ltnm3-2%tIn^w7D>9zQP3SA7|Mg{#skZ06XmfLxK&+A%cW;RuCOfwS!TEC7<L^JIha}ALIFKfNtCq7A2r9Wisf)*z+Z}eX-zA3?ewz=h>5_kiu80k=h8+RFA;6Uof;X5oDDHzY<JUn@KYN>5s-nTP)2LSu9SE@e&7;%GG4N9TcBA#6&W|NE2ZEa&#=?7*<H>hHNiX)2W^-fQoHx=9_5oumW1ykc1^2+3Cc&Xkt24MjNr{2bXAuFNd^;{tBbJ|&fKb9XpKV#5xeSxxIS9RlHI=B!qH2upsuY@Sl$RQ9Y>I^V(VRqq@5?1MS@A}Dl-Tk*ldXk5S3!rqS%$dTgpbwM>cjP&?^K1w1`)5+5&1i2yUQ1{_Fxna`jw?Of+CHy}5`%%^Gu+P+O=5x?1+^x__cL<GKe4!~pUWs2rtq?ULdE+RRj>-HSscwRmzWY@Bv3Q&q3D2k3TSBlXD5oltfLs%<xNNc-8fJ{logg7)9oa4LSp-ozuGEK$NtDU-UdVoG`%8`?`B90Ete!MEIhm6Iy;a<s98os<f3*rekI)Rp;lGY2&CYNxb}U7=LFv%}ba+1ni)rc#jI3?i(cB$-yAVrdE{xOzeyB!qKGWEfO4WM^fPEp&vF3c{&m6*q%Svsc{f4l1vkR-!cuzzZk_F^wrhsOG)8D3<D|8^dJ6s+dxzl~zf6GnsCNpjv2K(0WDP$j34ki5iIl`BDkyWJg>~$8@;rv$#8=Pe_8w!PW$p60&`<j<Kc0<k$+@u>d{`2cp}qplM(*1P%w|itwUPluB%HEv!Vguq~dDf^#BKBOs#2vkokuE()g59t?nrVl!NZ$y^9MpM54Hpbd-)H?pBhf^2bcq)e6TAs63qEs4?;o%S8%1)ob%(~&x$6v*^qkZUAG=1NRI(a}fG_UyWc*=onVnQ*u{Oe8GYDYPFr?&|oR8W7obbKu1C$VU>^jD{O6b_}k~2ED1W80e`)(Ao7^5)C8Ju!^)YNKpyrmcfxK2!q3M|0QEXHx~<FyTI~2!z7W>jvQbEB<s70AUSx0yPX<qr4grCoXSGwvJ_ytBVO<)1r9rjnk*S4B*6v^sbTD-krbNwP6fvyS-{B1Ug3OAqEdB*XV9TLIV9XD=r%Tl7C3;FJOe})&A&|8NZ<*Vg9wq_HLU4It%lzHMhlpflX8ufJ0YFja0KNiVkiu|#DwIZ=7tgks$}#}*j0l81U{bYBF!~5xUw;QbuN{tND$Bwd2dE5cQ;lbZ#+FG<B0e403C64MS=)tK@ces5xh$Vsp=-WlQg>#AnBc(bFwo$_^@QuRQ=owxtWxgN9?1?Mv^7-E|J0G5tw%yqJs6oj}YHbC+&W+tYjloxR!qJCarKHE6oV{a>!jZmMCq|s&qUD@L^fY5q3$T8k7<NKSdlO6(_C)(XbUXHeKvVcxE%H^yIZSS_<7epm9&c=!&C~iGw4U!n@bik^{V@JjV@UOoMCd^bwl7?K?dO&P$%$Gg_59b6+a(8?8m^kcq;1H_}C&5mU{i0}8j3i*aWvUT#~vst1iM%b+9i_zZO@PgxKjq%wwl9KsRkQ?6Pw(HRX8_!Xe6jgVS$M8{E^Aeo@ZggPVD1T#ka0QZz)AY7X#Zs_Q&Xb;L#X!3f<k1FVDrc~ijp>|4))J;EVL#fFUh0ttSsE3hYQ%fb9@wBM?3w331xk?L>xPN!*#{?*xb^~T>cz%?UfK_5mKnAa^jU`w_LN{o*W*$4!N+Vd(qf$O%5J@{sCC<G<$?O=`k%(nV6tt_UCWECx{t@h99ib6dx_ppIbio{=8O_=47|F@2ah{6NqvlH@4+$=NLa{V4EI<wePcK<BI+O9kOrovDp()Fz>_Q)jv~QuULtP^o?AttmL&|}>qSiS|6y}*%s(c=)-J7d4jE0bWx+x%2m*IxVVl$b1CJ4-+z^lt0snKo)_1N4bYgA5Fq=|wp;jug18ON4sF|Uy5+C{oH*bQo)I#CP@q?Ne~oCs(&Z8Fko852-^NN{wqx;U7|bkkup!?jLELXnncrb_p0U^aL$FeGZrgK;5@08gV7*4&v&NKrfB;2fn{y(DbzYiV&vz4J}ows}r4HOISH9mR4T^;yk%VLB<n%Bo_7_oU#p61-w;Nq2cnY3{bVD9m&~^Bi?SwYP_7+sO`!_lZ&vlN=ODB@Sru>TY&Uo+hNR#rPE(@3^OXoNJL$E<iI>pL9U@I_&}AHb#l!1R=x0VRXlJFr!5P$ebpjqSSp1OhdUT97IF_k08fLbtqDM>Of%y8i85QROY)K$4<WLu@uR7G2F$>iOg|wtV-bvvr8np?7kJYWnCVM-Gp6)^)j;b(MiQB1+->SS~#7K1HhLv0|O_Ju%6qP@zbJFhvf5T6P8i)(V^3MM*av4$eWpdYla||2P$u(=bG^l0L-r8vjeLee<WZylZB}Vd=z;~_0$A_uSe5}pbq#a7*Eee?wj**qH*GHGub-O!R@%L+y{XO&qifj#*aCHZrsw)^5W@3z)OmE+95p<-DnPLUz~&<P?WxT(0d1SsIFVfoOG~k;(?7}Ar`vRY9q#ndX#I<W4N&p4a?XaICbhnwj7MpVQwe~n}??=M^!J?(YmdK%%*0jq7sT}EqFr@<!@tG@jBE)^6GU}hIFWv$WHI>pSk#JeV1SwcA%F;FR%K+Jcdl6$i<9i-pklfUuYN7zcZ)YJTMLYsU3oW0Bb@xDM@8ypC4%xE&j9oU?JqAWJYu>P4wJbMe{uo(WupJgODt4lo3PFUaqUbbcVK=29#}KMu4yF1hI-q-HwI6F;IaGqs)M#fTaPAq^TiK*_d;-ym9i$brQN#4Jlor*vYbu9ionO%_-R`x?xa@#EB^Z!~m>-!1X}8mD)e?t87m9m`<s9?ZU~yEhFB{o|9E@a1<rY3bZj2nH|3pyqY^U16?t<E*p|<xrD~ljOPicZBR$fBl<EDLpVXl%RB~CkqLL|GBb5Ia0)y%^K=9Y+F`04+>R+8s<q@KcyN+$s%uMg1c4MVl!@1@#dTcTUu2Xzdw_ZtUI`!JG8t{KA0XM=91f)2$--hv2(sHl{GH9hW;KQ!v|rT?I{j|XW9m$X9{dhS-czGQg@EPEhcOyRNVGF8UpOolOUWCRBRFe3bYhx)r$*Trv^K{)Py#q=c5}BKVnlWII4z^K_GC&gsN#6L7J8qao(D(KW%_QIi(4=X0sFy+oJ3{@45TxeFb=+VbIGu_hbW5xeLzjxAo6*tUMtBV!4Dcwh6{dGsKiqOq+7^7Gs(~VDqw{~wPVxR6_5n8DYX3|@HbC~Q@9~rqC-u-n_LoT1UW1f$-#jWT@qL|L#)(W+Zc(`QQ-eM@11hB2dxt<_dbSZsTFpY>fw?0hw`wtFg4FAbjs-ag9S*<=p>$<F6(L*-?a!jO(a@i%)A=~!p6WIE#KyO%SLrLfun-|EEFKCTNvwzKsrVbD@U=9_}(K!^qD=eh}m4tCXn;*aF%3Z%wy6S2NzrMAAm4qNQAv4Nm|et$fRskL4BM}6l0P|77V^iQ^k!=B$whKGKAU8O77S0>HUpVL*7)e5Ie!S$$D;hjL`yGnI<IN@!YJ3?W7MY^9Ka7lx1k$95O?ytMybF62%|~85?D8V)h0vLycL>gn~7rw)3q{mdJiG_riocRY%eZ&xc6q>5?nbqNQMvfY^8Iqmn6aK{IMKd?D-Nw}rrWvQxU>M!gna0j7ynWk7^^!h#1JGbesa2P{cu;>VLz)OgKIRl;M&Y>Bghq$yoP&HLV@A2=jpU#0tuqBGYwg23XqKu;?Z?o^>QX9ymKar>AwG@JgRk6#H=*m8<ibi<Nqcv7_SZV6Zw_*@bYJc!?>&{14hA{dRvp>r<~M$%W3KzPf%oAhR`$QcM@U${H?gWa=7G(Mmbc2f=tUfby_F#Ci<F`7#Ag(kE-kD>ukEA$e^uy{g}L7T<dC<p+1;kbvY6tGNTwrxNq`l-_<!1QJRSPid|Kf0RlbagjWPaC?La^`!kn^At?v$W74IVfToUWKttbRkJ0Wa)PIayt9t@`;vE#_Lrm6BbH^u&s<<c(BSm=i-nW%$#J#)Y;BxMKcsYr=H#<Rmny>1aaY|0qf{i1L4q*F4aYArs1>!<gV3S_nmyzbM^@jG3rLl4J1Pr451RV<BMp?I3`{QXrMt2Te0GG%#ku`Pk&^moz={h@Un#kVn@Lh^%*93hy<!BR`iiU_U<XHg;5LJo0ZC{*`Z2I1h~YsBSio|0iLvO4qjaeZv(GlDxkUMK)|@suiJ{#4Zpq!{}0x=8ruiYHGN(CI=~t@C*);=-y1!&6>4vV>Y~6x+0DS@sJy1ZLioK5yGc)699acppQ{0RXcQ0N5YrR<C5l)~?3>3sIKE=+5$;(=Qp+mt*#e3;K$J@P#Vs_v9){F!D<DHy{I&xK_}yh<YQN;Wi^oKht?d}`W7Nv&?uyx39fnX@%uJ+GZDJrDTf4z3IB@|U+j($?v&%l}w4n9oBsPZMs_XV;1W`B@s!16G26>o(`{`OQCe5g$Dw=|S@f%kl=RCr&(k)@j{T`OlTIdGI;J~sinXX!>0Fkn8m9*69QU&JXQFa8Gx2!Ptz%X{Qse=RekRz$<;`)0BPU()+=;(S7&IMuuEGn%;pLrPxH5WsBz<1i1?5+soYUYW@V-Ouaiy{Pm(v>agB9SJ;rXI+`@oHiGLs^?%KX@|yh0K`?EE^N$^aT{CfnPMggG$lMZb*p+6@w(aQ-<DRF+B~Uk~|dLDtxMwppFnYg5XUBVC?8Q?gpk>n<E=)qryb0B)j`X3S9<uytIs6_2V~N4-ZRLz(7kmd>fP};!4c#rh}v!?!ev}V_W_5l4>%$IrpY|(dk5IGL4m&E_irG_Ljb#8<5a?p2MXMvohD)7^!(sQ-*pLKu{Sqxd4w)DwLw&!TSgjdx0<Dg)uzT0TaSxr0k7$jDB4<Fpg?r$stdNsKd(k0emkW0ri;o%9sn;J5lzI!J_!FxvE)AO!0~k?D|3GSV4#fZ?YB0FEQBYqwO?RwlNGk3<e^(n3>U<R`;lI#o%my7Y=W3fcqtI$R16h;Y$>P9ir$)vYE%5eUeKa;5C$R+UA!lxWH)sog^{4z_TQeIXFu#$uG~Lyj++?c5`?cJtWG*nK#Z*ZKGOGahKAo=N&<@*5W-LObr4;GS8Htw;63)nCfyrm4PmqCOImSV<s-hRFd;Lnm{X1{>EYrUMy7esLxan1$lUa0RrCI;<i4eoZaalh6}}zMFHQW&rFLAn3qwj>^BwQN<fSYLxWTl@D`rbl!j(we&nyvB`6lfKag$!=bb`?!D8@wDI6g)J!8Kbi1(KyhCHduHAal+vn}|nwfXG_i%U2gnLb{AFP`RNKuZ~^-#tia+4VBIKzP@K9X!K##A$uYqaj$Bcjs*y1oDgr7r!yezzr{x0hc5HSO?+&0zn^bXK)BPBs8I|%1Tx$?K_h{8P!@1X=%?hsSzO`^+h}?d&o;LI1Og1m;ojxCA0M)w0%RjZQ%tj$pug)IV2FPb9b0}XGOB}1{IC}__sW3##OAvF94FZZ;oE+3=tiW)i6#*{76y+M%X=g)oo|}?iF^D*=N$p)w7s{<unNVRzO;fi>J0XDyuGS94c~-ws|znjYiT{*M|t6R)W}Ku7m2q+Klnj<jHto=Js-h&7#7V_7PwD1t5&mxNt?dQ_R6i);wgZ;I*H=A}r+fC-$wBhITimNKHsvw55qjaJ(=@kviCEUV*_7DO4QDR`ILP2rVj>6x-IoS6wWSf$hoj$L?o1#FKd+v*|QwAvI+g;Z!ix2SDcTj=3y85Zh(|q4|03@mJ#+#qZy#ut0PJX<RVq%_|bA-?PaQXa;J5aA~Xi-5F+^ba=*68Jwq;j%RX7y*OHxvu+O{IWQu@C)S3>c4NeHapQ3_lHvDB^f67?iApF`8BOoj3PM!}Z+;gc*=_U7SX{gU%iEe^pthQ=AgZA0qo<|d<RS{nN-%@i^6DI%Gc{zed&==<lZxNHHkzx|s4<AaZcd7%iB_N|GPIF?2Mi;Kn^PV}4B+a}JAw}&aK8YJ*5D}UlTtA#XO7OPC)u(#fEO$sDEs>LKYn=+P7`Ow0xQgpVR~%t?x13%h6-Sa?4UIc_%ek<$uy0Wu?<hlECmPNNT$a7v>NmMkV(uZ6Rz^qxbnpogiq2oPBwv=v`VoeBn6cKa9421;z|H-xLU!vXo4}Y;{jd(ICp6i+%ZEg`d6hq=l51_Od6VmBYmVjL!TemWi|hu;kOwYD8SK-2@^-qh471oQg+#vVkztb?}8aG>EL1M67Z6z&FLBae&ng!g!|<!SOrEn%NR~IDesqbk&!VBNXwX7?lPiysWB{XH@HOnx47u%PSWNf__X`&1gm2zQd81q0A;2;X^PT4*hk^j1r181jagv2LL@~DwvAUyG{4Tv?@`5qd9s1>;^LQk?3)1@y6uG!l`g<aj!@i30c9egPiB4BK8nQVl&Gz{BslyDB2fLx1AW@Bk}@X?)&y2V3ae<MdM2puV03<$4adpLRIs7|H<{8$c2(2U%e*vC(J1ee><V>x%O2#kd-8^@(W|_U_Z=;}-|Y(Dzz9%LjL(D?4c1IMpu6u$1`S^80J}T9ac|l0n=u&bd1}8>k9TG@!*`9$%`X}QzxM0zh@Y@LOh7Zez4F^-yuM5p*nuN>QYLfhyqCwWmAYQ7_*nvc3fq2nlb)BfHjPBs_!P!1kJg(z28mom2UK^DMezWh!}Syb<IjaEH`D=n^5Th6dO*zT5KAS8ldiuqTSRAZyt!?$Xm1{nGWSKpg|{2nd2Z&TR>yD*VC@iyyllzSQi}(#ceZnf2Ty(p5$=q<x4Vw#0#_dOPb#x6LV${qwZf%Ac^IiD;52eCZRE`yo`xsFX=S_}h{jiS(Q<Jq*KZ0&p;o)dN(fcEnOnbDa50&&G2P?dFap)hyZHrL)wMLzEoZ?veWkBsC~>C<p1p3qKJw2&5f#4)iI-4-;dsaZzJx)J%*-^XAO?Z7$?qGWq~8Yc)+mFR<Jlj^@G0)Ykot9g^zZ4U^EQg&VknIW=e^DEq^i8b&?A=Mle}}lb*+tL$}i|Gv>c7pHfPP*KbpD+J?B+o+}2Fu4ucIID1TvA*3}sT4h-diSLqP=gNPB-`(38wVOR_IY&%hwU%dmu%n%)wg9V-59GWSh%aG65AFATjEgarC#^@BYu^RwNIy9<771xRn=-6N?u6~mPFO5UJAU)x|^f(Me`3<C<WWrz^5j;RKvESf#P2yAB^DMzmWt4g(Gw7K?QDZT@%+0gngcQ`y#|!GVT*hpw<e14|vIbrOz-yN<%D^_e-)#n>;La4mh0<tzR`vUv923JBo8$It4R#k-csYn0$8Pr!eipypmgXdQzM?l0MI(a*t{%wr%W>qo$15&*1rx;$pju-lyx^Pm%HTluF{Ii(a|`TmPYJFKSM3q8>UXa4>Yc^!+W?#*(WK^1flngLyp7*!0@iw9MujxU9UNdYznP5r0>3-#^^1R9PG6-+b4?iR$a+oSVUlW-M4Qx0q$Vdj8$h@~V+M*LKqmn$3V>QbWCAx97@B~f1(G7*fB|d_Oh-VL0vQtls(>#AE^D$>0k{jqP{flaw-Lax0J8>8G(epJ7YQg(AWb9072uc&sRSx8k~{%sog8o=nJ29@f!v8Y9ns(kxkdDO;%rCqIpW9@1Ps7WAaf$uJF&39EJk>GLW7a(eUwuU{A9pFBRw|>)BxZHEGnXD6EhC1Qb30z!#Hu)fMx~4JOD@mkPg^SB;_W?8qm>+j7^MflEp_}cS5>>mVFE%3-D7U{w9+YY2v{9P8>Rdn@7fYl1`JP4~XdGf&=w@<QylD7^vaMHU<JZfSeQA3?O9yumh(Rz`#HmM%ryM>w&tT_|*p_*hKFFyBE0rz~u)REwaZG9F4^GMD!<e7_rSGd^>^hi6{r&`H1xgMmW&U6VeUnc4Yh}v>!nGz|BT7H?ZZCunWv)7)%JjU_i3NXhc8`14bC2`~cF2;flbohv|$kWe}+F0C)y$GElAo01tR@z<(pPI1%cADh3KP3<d<;I}pkN(hNj>q;ikE_>qjCAmYRyC!Zcz?n&v+@sz;L1{OSwVT5ssF#ZuZ;(+c32sZGU0TB&kdKeW6vju_DjWqXUye9>J9F3U3@MQ7lFvA?*nQZ19t_h@n8084F1_3dSRQe=@;~>mrwkPU3;m?VL&*7ap9dJZ$C!{`U^MH$oX`8^t2L?7U!(omh%*_NiH(>GsD35ggoJfkS^h9#Q*hw6Em~$xse-A@AVW=r!;$Z|NK-_a^Nx|z%@H$bh34%?~YcdZLQ=1stL|y_k8Q9rC!37pGPz(V&31Crx(;^@<k+Ba5nj?P}Ns5UBMzV2Y9RXO1V9aEvBD*w^tiVwP+AaV?5lsr*M!?1<$r?b>0Ch%SWP(7GG@bm`gku7vGI78o#WN|^N&g0ZIWS5Q!<~#%V1fhQ7O>|4vIXopkjQ}n49HJpb0*Xqn6SVr25>s?f`RIN98nJNWWYiLI2Q=i0O1BKDsX85G7hv-Acq6K7;x4|W(C50B1nOb4%kn`<R->CvC+Vb9U-<OTRdsqiReyP_ECT=;!hLz3rJF+i39B$Xy}M+PR2N(P64G4fM`I21N<C0$B85cc6h>#6N3&Q=MmYQ6y;>FC#e;%zyKHq*e&wv0k}WXS0Auo6TA!HUS#?wksoNZfF1{CG!omB&kwv|gf=I68=&z3DF@^^VEvI2p3HNgbOXR0QNPLU2iQKKvw_W>g!u&R0<-ykEMX#plgOS^5feC!7~v%HBS}A}DgwM7<}$)~K|sPI+!>I`0Ju*2JAlCf{0-1zgr_H}IQh>x6)-99$t6!XbAt4f#0`jeK;k2WIN8TQrU!^S@VQ|y<%rBq27FFm%rS{M@-gAzfa^~1HG-Lw3=J%LoDP`-1rg1i{C1$a1Na_hA_68H82mWMFei5cn;GV6BIrLyIl`zx#EU17{sA9+oPi0rc7R<2{uy}iFuD__10r}kIrV^?Phflw+ypp2u&_sL_&7^3$7Uv<8xZ-xk_WOrj3Y%<dQ!Pz?j%e+gu#?RzK0Q<Fwhhj@h}1s816Z;q~LXP<@LyM1q$p)c}?PBLTZyio6t*uCIdSg7`VV<Mv7rFCjl%9WLgAdCNUNXnhBppl47EO0c(s<$0U{_7c=px2ro@4E1*=7w41<Cppyc(5xB9yu|`mIBApQz2^`P_O#{Of>6l5W1ROAcJb`5m6maC2C#)1`+(0`8HaL)O5r3Xg+Yx`BMDnBnC-XDmoC)<#DlD*yft;SSV1#-fC6og=8Hmt;&K=>?iQi5jYC>rfE)I~=B!(xo7#Y?{V+F80fJc#up0rQE;{v%FV9~&dMSN{S#UEh0lg*u+?Bn!UfS&^H7m%dL5>L=Kz|aBN41{q&odQZ9=+Hm~NA!83j)6&>*zm*~Cj&h>&WUUWj&dT{fzz5?U|<Xb>K1YJN!(BRDw1G<x(nQ1g!(6xA7HdV9S3YQ@Y;dT55!@lHUoA$k@0{i2i!Sg{gV)$xbsAGC%8Q!zkuvd&VEv}6Uq%>c_8cpu^FZkA}%;#>@XJ*c*6h@1|&a%^kK4Mvg=_oBg_{BAbj$j6PP^mtrPr?Sn!eiow(v;rX!*_(a$gt5cuu@B?mGyBJ=^porHMg;U|L_(8mC#2ZTEix?wgYV6%Y%j{_KURw4|31d2G4yAy#8d}aVd1CSo3MB-GzgmeSQ9q{gezke_qF_Gbb;)ijDFv2qt%{XCmr2UTr9bwquNErtveKNvx3ML@iN78k|pGP2k4(`mkfC=1AKz+jI0TvIFHUWzd5NrU3!xTlBnF(xeK;#2X9?1GIj}%bpz~qLhlQ8cP#!>?P9!GHIL{o&s=Kx56xaY`{g4gWI>yhS~EZAhdCh#yxwLqdx>LqfM6P^tuT*NUa#t@*B$QDgNEfO-38=DACWYCTzMF0W=-WZ^cz$^tYCK6PWUy5ATgsB2?7onjEC{1o-qGOX}otWrEIwvV|M1TTg8tAP6#RNJf;DCYP2^ecYfFr^@DWyQ-M$RdK!4q-|<a5N?CixtJ<Ou>s-e;mYk?WmYSYQ?dJRPCI2=+e4DF=WufT4k&3l!=^a3>Ts$+XEBPfRIb!|&<EfU*W6E1>0pJPKfRz<nYjHv!hbj0RFHB5V^Y4!~}pbOSE?I6)TZr-1wgCMgib0r~v^h(4m5N6t8aPDhe{5~7m~p4juG94C$#xZ!{{1_U~goPpU47-b}|C#Dt1z(5%W(k&3{5xYOqR}lmY++8I1Cf6Uh{J^3G_Bev0li7}p{zMJ~wi)2tz>Eh<IWW%w?GK=Egq|m<8xZcm`2})6;`WoD4S?>X%mZdO$;~;PFp0qcWxpSdn7CnJ2m_NJIQlSI5fJt`nlUE~A`~6~&xuS1#x?NY0S}J&??e^@KOI2DK!Aq9fCzg}L^%MOfv68mZlJ}FXnf!hkM!fj(<jM28QnRX61dq2g3l3*Ff0*fKO#ju$=yJ}20Am~p%F`;b0T55AX2)M<_?VaWZ;jJ5rG#zlK69;VGi#EGBZrqOwvD$a)e=nz!(QAJs`qi3?>lU0dx)UXMn=P08g9{2-NMQ)dP1PsPQm#6Cn8ySlA;v97Za__)K7QCon&;@(J0`xugk92PpSAdJ?7{!eq+C-@_127;6fcc$fkS8221odj0W7Z!W(+IXk<$zPdj<dwO^I_Ups9mp?pv{_#g2U44D>_VW5E|9O3Le{yyG)NjJl!#{g|dwz9ydGbHc-+q7j`R(n^?b9d!{rca|-(I~r(Z^5juKxY<$unKz_U8NRH+QF8_S5U{zx(R)_Uz{Cv+>(k?=SxPh5vr`#pUU%SLyXX<dd&&Zcnas!;`<>o?m}+`E-2YlVyJN&41vd{_#wAIJ?kK_owr3|BzpN@$&Tj{eS)I{Nn!G(^q%*x8GmffBOCPyYq{0pFa7`)f2Yj)z$N>Px9+$y4{cOl@9MOamdfKl^^}|17Ey+_Wb?Ry}Ht!Ucb8f`~3R!{@Yhq^I=|%mHq0+A7BSBZm!?mTwUK^-kzox_orXH)Gz<?r>nP@C-<NI=m|NrkM8gJ?iVkAwY=6-Y1Mxm+y94G_xHc~%~JoH%<${aK3nr2!>{|pZ<jiM`|ztCKeuN8cKnK^y$b8|RbPZ(d~&+5BFWJgFaPeedwD<Eqs4pV$Ni(9S_|icmv8SbfBqZ4{`LCUmyS=bzdvk%e~z0Fp5ViW|KLfF8x^<YxB)!rqigfsKKZ$4`txbxU*q`x{Mi>TPtLF3JaUdliMh8x+&>7!pMNSbKl#9)wdpVJ$Ii33+YdHwe9PMHzyA3HVa;0e{pq~L>kse9e~!QY>+(}+_}4Y=+r#g+u)z<%%I}Y0n4Zh)i`yUG-4Dz2?(I2(^Vj=mf95a$?sTa?wLE7pgQejM?|1Lnua*~g%i(T4-0}a3;6aY>^^0?Ib^l*U$LT9=_;*tG`?J5?+`gH&dX@j->bd?w@&9-{jU?TC#0X5?|K-j8@wc9z>vCg5zto26KYp~OUusKVJ-zzmZh58y{?Xokxjp=G9e<3Q{AGUl+s5C*A9#w}*Tb@Wbboqr^X`XF`Jbof-owkg@84=u9yetlNXO-a0CKGt<9nZ;zk7Fi{pRV@_v-S&0}Xk3#(zBf*)KfutV5>$1K;~_8!UGJfp3Kky1yTv_eR`*e|vp0zBuIc&1I0&i>r5ky1f0dpoZW6=Op#bhmtC9e^~y8q|SRiCaQ1#e~M~(R8;FPiE8@=QSCn~s&D?^i0YfaDXLfhX<5DcuL|qce@$Ai{uAPQ@%H@g?&Kwo<ns3UV*wsNU!A`_`!_`Mba@}4T#T=r>Q3<kzxmD8v%gm&_>)jexzDfq;aAJyR~@k$S9^DR^G<^K!_&9t-+lGw{N(QW-SNuL#$v8y@-M%;ydKJAq5JWrr*gdiD!1YP4H^C5Z#ugfY5e#vpK0aKzq-9V|Ms!(*&jR7_|X<9w*2WD9=7(kpa0=^Z*MNX{pXuEm%sn|_j=s(`>UJlN4MIs^tj6T<=v^i>Ui;Q*py*8KL6{*<vXtY^z0v>{j#@w><h=m{^4Ga|J{#P`}Aj?;@R^QzIpi#=k&-|AH60|>Mxvq^~1fMcB%7HKl<p)FaPBiF7xq6*XQ3|o<4aH;*+QP__HTJv%iP;hj#W`3&+nq*rz`|m|?mfzJ79bcXIaI=kG7}aO<ah=tJ3f`1eC#BuUmOTwQ-NW#?~4HX{I>0``-OJp1g!$mjXbeD0IeqilT|?{{47r`F16FFyR`)Ic8Zc$?ERr9fQ${$`wDSiy^j%U>$<diTc*jKR})hl^*@*?+%!H$QmzxhH!4vm5p^zjuCkyz8^)7;oFTu{IVtmK#3wm|pSYgO-f%`+o0tYySD;2PwS^LUa1&>O!Bnzde0agrEIiUv;95' # noqa
        f.write(zlib.decompress(base64.b85decode(b85)).decode())
    # noinspection PyUnresolvedReferences
    import mini_pyaes
    os.remove("mini_pyaes.py")


@dataclasses.dataclass
class MatrixData:
    # only used internally, not part of matrix json data
    index: int
    out_file: str
    decrypted_sha256: bytes
    # part of matrix json data
    filename: str
    size: int
    iv: bytes
    mxc_url: str
    decrypt_key: bytes
    encrypted_sha256: bytes

    def __hash__(self):
        return self.index

    def __repr__(self):
        return f"[{self.index}]{self.filename[:8]}: " if self.index >= 0 else self.filename[:8]


# noinspection PyPep8
class SortStrategy(enum.Enum):
                        ####
                   #########
              ##############
         ###################
    ########################
    smallest_to_biggest = 0  # fast first

    #####
    ##########
    ##############
    ###################
    ########################
    biggest_to_smallest = 1  # slow first

    # # # # #
    # # # # # # # # # #
    # # # # # # # # # # ####
    # # # # # ##############
    ########################
    alternating = 2  # maybe constant download speed

        #     #    #    #
       ##    ##   ##   ##
      ###   ###  ###  ###
     #### ##### #### ####
    #####################
    chunks = 3  # constant download speed


class MatrixDataException(Exception):
    pass


SOCKS5_PROXY_LIST = []
DOWNLOAD_BATCH_SIZE = 16
DOWNLOAD_RETRY_ATTEMPTS = 15
MXC_DATA_PLACEHOLDERS = "https://{}/_matrix/media/r0/download/{}/{}"
MXC_DATA_SERVER = {
    "mxc://nerdsin.space/": "https://nerdsin.space/_matrix/media/r0/download/nerdsin.space/",
    "mxc://anontier.nl/": "https://matrix.anontier.nl/_matrix/media/r0/download/anontier.nl/",
    "mxc://waifuhunter.club/": "https://matrix.waifuhunter.club/_matrix/media/r0/download/waifuhunter.club/",
    "mxc://midov.pl/": "https://midov.pl/_matrix/media/r0/download/midov.pl/",
    "mxc://asra.gr/": "https://asra.gr/_matrix/media/r0/download/asra.gr/",
    "mxc://cuteworld.space/": "https://cuteworld.space/_matrix/media/r0/download/cuteworld.space/",
    "mxc://matrix.org/": "https://matrix.org/_matrix/media/r0/download/matrix.org/",
    "mxc://lolisho.chat/": "https://lolisho.chat/_matrix/media/r0/download/lolisho.chat/",
    "mxc://cutefunny.art/": "https://cutefunny.art/_matrix/media/r0/download/cutefunny.art/",
    "mxc://matrix.lolispace.moe/": "https://lolispace.moe/_matrix/media/r0/download/matrix.lolispace.moe/",
    "mxc://lolispace.moe/": "https://lolispace.moe/_matrix/media/r0/download/lolispace.moe/"
}

"""
cuum.space
halogen.city
matrix.kiwifarms.net
nerdsin.space
nltrix.net
tchncs.de
"""

logging.basicConfig(
    format='%(asctime)s %(levelname)s: %(message)s',  # noqa
    datefmt="%Y-%m-%d %H:%M:%S"
)

NOTICE_LEVEL_NUM = logging.INFO+1
TRACE_LEVEL_NUM = logging.DEBUG-1
logging.addLevelName(NOTICE_LEVEL_NUM, "NOTICE")
logging.addLevelName(TRACE_LEVEL_NUM, "TRACE")


logging.Logger.notice = lambda self, message, *_args, **kws: self._log(NOTICE_LEVEL_NUM, message, _args, **kws)
logging.Logger.trace = lambda self, message, *_args, **kws: self._log(TRACE_LEVEL_NUM, message, _args, **kws)


logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


class DownloadManager:
    REPORT_INTERVAL_SECS = 10
    METADATA_UPDATE_DELAY_SECS = 5

    def __init__(self, to_be_downloaded):
        self.__start_time = None
        self.__batch_job_running: bool = False
        self.__flag_stop_metadata_job = threading.Event()
        self.__flag_stop_progress_report_job = threading.Event()
        self._downloading: set[MatrixData] = set()
        self._failed_downloads: list[MatrixData] = []
        self._partial_download: dict[MatrixData, tuple[datetime.datetime, int]] = {}
        self.to_be_downloaded: list[MatrixData] = to_be_downloaded
        self.downloaded: list[MatrixData] = []
        self.replace_existing_files = False
        self.ignore_even_if_nonexistent = False
        self.mark_failed_as_done = False
        self.metadata = Metadata()
        self.metadata.apply_to_data(self.to_be_downloaded)

    @property
    def progress_report(self) -> str:
        files_percent = round(len(self.downloaded) / max(len(self.to_be_downloaded), 1) * 100, 2)
        total_bytes_to_download = sum([i.size for i in self.to_be_downloaded]) / (1024*1024)
        total_bytes_downloaded = sum([i.size for i in self.downloaded] +
                                     [db for st, db in self._partial_download.values()]) / (1024 * 1024)

        # this has a terrible accuracy for unstable networks, but its good enough
        total_avg_bytes_per_sec = sum([downloaded_bytes / max((datetime.datetime.now() - start_time).seconds, 1)
                                       for start_time, downloaded_bytes in self._partial_download.values()])
        eta = ((1024*1024)*total_bytes_to_download) / max(total_avg_bytes_per_sec, 1)

        bytes_percent = round(total_bytes_downloaded / max(total_bytes_to_download, 1) * 100, 2)
        return f"Downloaded files: {len(self.downloaded)}/{len(self.to_be_downloaded)} ({files_percent}%); " \
               f"Total bytes: {int(total_bytes_downloaded)}MBs/{int(total_bytes_to_download)}MBs ({bytes_percent}%); " \
               f"Active downloads: {len(self._downloading)}; " \
               f"Failures: {len(self._failed_downloads)}; " \
               f"Avg Rate: {round(total_avg_bytes_per_sec / 1024, 2)}KB/s; " \
               f"ETA: {int(eta // (60*60*24))}d{int((eta // (60*60)) % 24)}h{int((eta // 60) % 60)}m{int(eta % 60)}s"

    def __print_report_job(self, init_shot=False):
        if init_shot:
            logger.info(self.progress_report)
        while not self.__flag_stop_progress_report_job.wait(timeout=DownloadManager.REPORT_INTERVAL_SECS):
            logger.info(self.progress_report)

    def __save_metadata_job(self, init_shot=False):
        if init_shot:
            self.metadata.save()
        while not self.__flag_stop_metadata_job.wait(timeout=DownloadManager.METADATA_UPDATE_DELAY_SECS):
            self.metadata.save()

    def __decrypt_and_save(self, data: MatrixData, encrypted_data: bytes):
        decrypted = decrypt_data(data, encrypted_data)
        data.decrypted_sha256 = hashlib.sha256(decrypted).digest()
        self.metadata.metadata = (data.decrypted_sha256, data.encrypted_sha256)
        save_data(data, decrypted)

    def __multi_progress_update_callback(self, data: MatrixData, downloaded_bytes: int,
                                         start_time: datetime.datetime) -> None:
        if data.size > downloaded_bytes:
            self._partial_download[data] = start_time, downloaded_bytes
        else:
            if data in self._partial_download:
                del self._partial_download[data]

    def __check_all_downloaded_before_init(self) -> list[MatrixData]:
        """
        Checks and ignores files before starting the download
        :return:
        """
        if self.replace_existing_files:
            return self.to_be_downloaded

        filtered_: list[MatrixData] = []
        metadata_keys = set(self.metadata.metadata.keys())
        metadata_values = set(self.metadata.get_flattened_values())

        def verify(__data: MatrixData):
            if not file_already_downloaded(__data, self.ignore_even_if_nonexistent, metadata_keys, metadata_values):
                filtered_.append(__data)
            # added = False
            # if self.ignore_even_if_nonexistent:
            #     if  __data.encrypted_sha256 not in metadata_values or __data.decrypted_sha256 not in metadata_keys:
            #         filtered.append(__data)
            #         added = True
            #     else:
            #         logger.info(f"{data} Skipping file {data.out_file} (already downloaded previously)")
            # if not file_exists(__data, metadata_keys) and not added:
            #     filtered.append(__data)

        logger.info(f"Starting existing files verification with {os.cpu_count()} threads")
        with ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
            for _matrix_data in self.to_be_downloaded:
                executor.submit(verify, _matrix_data)

        filtered_indexes = [i.index for i in filtered_]
        for data in self.to_be_downloaded:
            if data.index not in filtered_indexes:
                self.downloaded.append(data)

        return filtered_

    def __init_download(self, data: MatrixData):  # -> bytes | None:
        """
        tries to download a file
        :param data: data of the matrix file
        :return: bytes of the downloaded file in case of success, None in case of failures or file was not replaced
        """
        if not self.replace_existing_files:
            if file_exists(data, list(self.metadata.metadata.keys())):
                return None
        try:
            self._downloading.add(data)

            return try_download(data, self.__multi_progress_update_callback)
        except Exception as e:
            logger.critical(f"{data} Download failed: {e}")
            self.to_be_downloaded.remove(data)
            self._failed_downloads.append(data)
            if self.mark_failed_as_done:
                self.metadata.metadata = (b'\x00', data.encrypted_sha256)
            return None
        finally:
            self._downloading.remove(data)

    def begin_batch_download(self, save_encrypted_only=False):
        if self.__batch_job_running:
            raise Exception("Already executing a download job")

        self.__start_time = datetime.datetime.now()
        threading.Thread(target=self.__print_report_job).start()
        threading.Thread(target=self.__save_metadata_job).start()

        to_be_downloaded = self.__check_all_downloaded_before_init()

        executor = ThreadPoolExecutor(max_workers=DOWNLOAD_BATCH_SIZE)
        try:
            for data, _encrypted_bytes in executor.map(lambda i: (i, self.__init_download(i)), to_be_downloaded):
                data: MatrixData
                self.downloaded.append(data)
                if save_encrypted_only and _encrypted_bytes is not None:
                    save_data(data, _encrypted_bytes)
                elif _encrypted_bytes is not None:
                    self.__decrypt_and_save(data, _encrypted_bytes)
        except KeyboardInterrupt:
            logger.info("Captured CTRL+C. Finishing downloads then exiting.")

        executor.shutdown(wait=True, cancel_futures=False)
        self.__batch_job_running = False
        self.__flag_stop_progress_report_job.set()
        self.__flag_stop_metadata_job.set()
        self.__save_metadata_job(init_shot=True)
        self.__print_report_job(init_shot=True)


class Metadata:
    """
    This creates a file used to tell whether something was already downloaded.
    The data contains the sha256 of the decrypted file followed by the sha256 hashes of the encrypted file(s).
    The sha256 hashes of encrypted files is a list because a single file could have been uploaded multiple times.
    It looks like this:
    {
        "aea5pNeCuFefGCuPU/lid1e2Veki6djP45ntIYQFl5Q": [
            "Q5lFQYItn54Pjd6ikeV2e1dil/UPuCGfeFuCeNp5aea",
            "UPuCGfeFuCeNp5aeaQ5lFQYItn54Pjd6ikeV2e1dil/"
        ],
        "1e2Veki6djP45ntIYQFl5Qaea5pNeCuFefGCuPU/lid": [
            "e1dil/UPuCGfeFuCeNp5aeaQ5lFQYItn54Pjd6ikeV2"
        ]
    }
    """
    METADATA_FILENAME = ".MatrixDownloader.metadata"
    TEMP_METADATA_FILENAME = ".temp" + METADATA_FILENAME

    def __init__(self):
        self.__cached_metadata: dict[bytes, list[bytes]] = {}
        self.__read_metadata()
        self.__changed_since_last_save = True

    @property
    def metadata(self) -> dict:
        return self.__cached_metadata

    @metadata.setter
    def metadata(self, value: tuple):
        # decrypted_file_sha256, encrypted_file_sha256
        key, value = value
        # appended "==" because:
        # https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
        if type(key) is str:
            key = base64.b64decode(key + "==")
        if type(value) is str:
            value = base64.b64decode(value + "==")

        if key in self.__cached_metadata.keys():
            if value not in self.__cached_metadata[key]:
                self.__cached_metadata[key].append(value)
        else:
            self.__cached_metadata[key] = [value]

        self.__changed_since_last_save = True

    def save(self):
        if self.__changed_since_last_save:
            self.__changed_since_last_save = False
            logger.debug("Saving metadata")
            self.__write_metadata()

    def get_flattened_values(self) -> list[bytes]:
        # [[1,2,3], [4,5,6]] -> [1,2,3,4,5,6]
        for sha_list in self.__cached_metadata.values():
            for sha in sha_list:
                yield sha
        # return [sha for sha in [sha_list for sha_list in self.__cached_metadata.values()]]

    def __write_metadata(self):
        _file_path = os.path.join(os.getcwd(), Metadata.METADATA_FILENAME)
        _temp_file_path = os.path.join(os.getcwd(), Metadata.TEMP_METADATA_FILENAME)
        try:
            with open(_temp_file_path, "w") as _f:
                base64ed = {str(base64.b64encode(i), "utf-8"): [str(base64.b64encode(k), "utf-8") for k in j]
                            for i, j in self.__cached_metadata.items()}
                json.dump(base64ed, _f)

            if os.path.exists(_file_path):
                os.remove(_file_path)
            os.rename(_temp_file_path, _file_path)

        except Exception as e:
            logger.debug(f"Unable to save metadata ({e}).")

    def __read_metadata(self):
        _file_path = os.path.join(os.getcwd(), Metadata.METADATA_FILENAME)
        logger.debug(f"Reading metadata from '{_file_path}'")
        if os.path.exists(_file_path):
            try:
                with open(_file_path, "r") as _f:
                    data: dict = json.load(_f)
                    if type(data) is not dict:
                        raise Exception("Invalid metadata json")
                    logger.debug(f"Parsing metadata...")
                    self.__cached_metadata = {base64.b64decode(i): [base64.b64decode(k) for k in j]
                                              for i, j in data.items()}
                    logger.debug(f"Parsed successfully")
            except Exception as e:
                logger.debug(f"Unable to load metadata ({e}).")

    def apply_to_data(self, data_list: list[MatrixData]):
        """
        Writes decrypted_sha256 from metadata into MatrixData instances
        :param data_list: list of matrix data
        :return: None
        """
        # {123: [69,420,42]} -> {69: 123, 420: 123, 42: 123}
        inverted_dict = {}
        for dec_sha, shalist in self.__cached_metadata.items():
            for enc_sha in shalist:
                inverted_dict[enc_sha] = dec_sha

        if not inverted_dict:
            return

        for data in data_list:
            if data.encrypted_sha256 in inverted_dict.keys():
                data.decrypted_sha256 = inverted_dict[data.encrypted_sha256]
            # else:
            #    logger.debug(f"not found encrypted_sha256 {base64.b64encode(data.encrypted_sha256)} "
            #                 f"in inverted_dict.keys()")
            # elif data.encrypted_sha256 in self.__cached_metadata.keys():
                # shouldn't be common, this happens if --update_metadata is used
            #     data.decrypted_sha256 = self.__cached_metadata[data.encrypted_sha256]
            #     logger.debug(f"{data} Possible duplicated being updated")

    def update_metadata(self, data_list: list[MatrixData], skip_existing=True):
        """
        Tries to create the metadata file from files that were already downloaded. Files are encrypted again and
        their sha256 is calculated to see if it was in fact already downloaded.
        :param data_list: list of matrix data
        :param skip_existing: if true, dont process already existing metadata, if false, reset the existing metadata
        :return: None
        """
        cur_dir_files = set([i for i in os.listdir() if os.path.isfile(i)])
        to_encrypt = []
        __keys = self.__cached_metadata.keys()
        for data in data_list:
            if skip_existing and data.decrypted_sha256 in __keys:
                logger.info(f"{data}rebuild_metadata: Metadata for this file already exists. Skipping")
                continue
            if data.out_file not in cur_dir_files:
                logger.info(f"{data}rebuild_metadata: File not found. Skipping")
                continue
            logger.debug(f"{data}rebuild_metadata: adding to the queue")
            to_encrypt.append(data)

        logger.info("Starting metadata update, this might take a while...")
        with ThreadPoolExecutor(max_workers=os.cpu_count()) as executor:
            for data, decrypted_hash, encrypted_hash in executor.map(self.__calc_hashes, to_encrypt):
                if data is None:
                    continue
                # data: MatrixData; decrypted_hash: bytes; encrypted_hash: bytes
                logger.debug(f"{data}Finished calculating hash for re-encrypted file.")

                if data.encrypted_sha256 and encrypted_hash != data.encrypted_sha256:
                    logger.warning(f"{data} Re-encrypted sha256 differs from sha256 coming from matrix")
                    logger.debug(f"{data} Possible causes: different encryption, tampered data, corrupted files locally"
                                 f" reEncryptedSha256:{base64.b64encode(encrypted_hash)} != "
                                 f" matrixSha256:{base64.b64encode(data.encrypted_sha256)}")

                if decrypted_hash not in self.__cached_metadata.keys():
                    logger.debug(f"{data}Decrypted sha256 not set. Setting it now. "
                                 f"{base64.b64encode(decrypted_hash)}"
                                 f":[{base64.b64encode(encrypted_hash)}]")
                    self.__cached_metadata[decrypted_hash] = [encrypted_hash]

                else:
                    if encrypted_hash not in self.__cached_metadata[decrypted_hash]:
                        logger.debug(f"{data}Encrypted sha256 ({base64.b64encode(encrypted_hash)}) not set. "
                                     f"Setting it now in "
                                     f"{base64.b64encode(decrypted_hash)}")
                        self.__cached_metadata[decrypted_hash].append(encrypted_hash)

    @staticmethod
    def __calc_hashes(data: MatrixData):  # -> [tuple[MatrixData, bytes, bytes] | tuple[None, None, None]]:
        try:
            logger.debug(f"{data}Reading file data")
            with open(data.out_file, "rb") as f_:
                file_data = f_.read()
        except Exception as e:
            logger.warning(f"{data}Error reading file ({e}). Skipping")
            return None, None, None
        if len(file_data) == 0:
            logger.warning(f"{data}File was empty. Skipping")
            return None, None, None
        logger.debug(f"{data}Begin calculating hashes")
        encrypted_file = encrypt_data(data, file_data)
        ret = (data, hashlib.sha256(file_data).digest(), hashlib.sha256(encrypted_file).digest())
        del encrypted_file
        del file_data
        return ret


def file_already_downloaded(data: MatrixData,
                            ignore_even_if_nonexistent: bool,
                            decrypted_hashes,  # : [list | set],
                            encrypted_hashes) -> bool:  # encrypted_hashes: [list | set]
    if ignore_even_if_nonexistent:
        if data.encrypted_sha256 in encrypted_hashes or data.decrypted_sha256 in decrypted_hashes:
            return True
    if file_exists(data, decrypted_hashes):
        return True


def save_data(data: MatrixData, raw_data: bytes) -> str:
    file_path_ = os.path.join(os.getcwd(), data.out_file)
    logger.info(f"{data} Saving file: {file_path_}")
    with open(file_path_, "wb") as f_:
        f_.write(raw_data)
    logger.info(f"{data} Saved successfully ({round(data.size / (1024*1024), 2)}MBs)")
    return file_path_


def encrypt_data(data: MatrixData, file_data: bytes) -> bytes:
    if NO_PYCRYPTODOME:
        return __crypto_operation_pyaes(data, file_data, encrypt=True)
    else:
        return __crypto_operation_pycryptodome(data, file_data, encrypt=True)


def decrypt_data(data: MatrixData, encrypted_data: bytes) -> bytes:
    if NO_PYCRYPTODOME:
        return __crypto_operation_pyaes(data, encrypted_data, encrypt=False)
    else:
        return __crypto_operation_pycryptodome(data, encrypted_data, encrypt=False)


def __crypto_operation_pyaes(matrix_data: MatrixData, file_data: bytes, encrypt: bool) -> bytes:
    logger.debug(f"{matrix_data} Initializing {'encryption' if encrypt else 'decryption'}")
    ctr = mini_pyaes.Counter(initial_value=int.from_bytes(matrix_data.iv, byteorder='big'))
    aes = mini_pyaes.AESModeOfOperationCTR(key=matrix_data.decrypt_key, counter=ctr)
    return __crypto_operation_generic(aes, matrix_data, file_data, encrypt)


def __crypto_operation_pycryptodome(matrix_data: MatrixData, file_data: bytes, encrypt: bool) -> bytes:
    # TBD: don't use hardcoded block sizes
    logger.debug(f"{matrix_data}Initializing {'encryption' if encrypt else 'decryption'}")
    ctr = Counter.new(128, initial_value=int.from_bytes(matrix_data.iv, byteorder='big'))
    aes = AES.new(matrix_data.decrypt_key, AES.MODE_CTR, counter=ctr)
    return __crypto_operation_generic(aes, matrix_data, file_data, encrypt)


def __crypto_operation_generic(aes, data: MatrixData, encrypted_data: bytes, encrypt: bool) -> bytes:
    func = aes.decrypt
    if encrypt:
        func = aes.encrypt
    logger.debug(f"{data}{'Encrypting' if encrypt else 'Decrypting'} data...")
    modified_data = func(encrypted_data)
    logger.debug(f"{data}{'Encrypted' if encrypt else 'Decrypted'} successfully")
    return modified_data


def __default_progress_update_callback(data: MatrixData, downloaded_bytes: int, start_time) -> None:
    """
    continuously prints the current file download progress. shouldn't be used with multiple downloads
    :param data: matrix data
    :param downloaded_bytes: total bytes downloaded so far
    :param start_time: when the download started
    :return:
    """
    if data.size == downloaded_bytes:
        final_time = datetime.datetime.now() - start_time
        hours = final_time.seconds // (60 * 60)
        minutes = final_time.seconds // 60
        time_str = f"{str(hours) + ' hours' if hours > 0 else ''}" \
                   f"{' ' + str(minutes) + ' minutes' if minutes > 0 else ''}" \
                   f"{' ' + str(final_time.seconds) + ' seconds' if final_time.seconds > 0 else ''}"
        print(f"\r{data} Download finished. " + f"{int(downloaded_bytes / 1024)} KBs in {time_str}", file=sys.stderr)
    else:
        print(f"\r{data} Downloading data:", str(int(downloaded_bytes / 1024)) + " KBs", end="", file=sys.stderr)


def file_exists(data: MatrixData, file_hashes: list = None) -> bool:
    """
    Checks if the file exists before attempting to download it again
    :param data: data of the matrix file
    :param file_hashes: optional sha256 hash of the data
    :return: True if file already exists, False otherwise
    """
    _file_path = os.path.join(os.getcwd(), data.out_file)
    if os.path.exists(_file_path) and os.path.getsize(_file_path) == data.size:
        if file_hashes and len(data.encrypted_sha256) > 0:
            if data.encrypted_sha256 in file_hashes:
                logger.debug(f"{data} Opening {_file_path} to calculate its sha256")
                with open(data.out_file, "rb") as _f:
                    file_data = _f.read()
                    logger.debug(f"{data} Read {len(file_data)} bytes")
                file_sha256 = hashlib.sha256(file_data).digest()
                del file_data
                if file_sha256 == data.decrypted_sha256:
                    logger.info(f"{data} Skipping file {data.out_file} (identical file already exists)")
                    return True
                logger.info(f"{data} Not skipping file {data.out_file} because of mismatching sha256")
            else:
                logger.info(f"{data} Skipping file {data.out_file} (file with same name and size exists, "
                            f"no sha256 hash found)")
                return True
        else:
            logger.info(f"{data} Skipping file {data.out_file} (already exists)")
            return True
    else:
        logger.debug(f"{data} Does file {_file_path} exists? Nope")
    return False


# TBD
def __get_socks5_proxy_context():
    urllib.request.ProxyHandler()


# noinspection PyBroadException
def __download(resp: http.client.HTTPResponse, data: MatrixData,
               progress_update_callback=__default_progress_update_callback):  # -> bytes | None:
    assert resp.getcode() == http.HTTPStatus.OK
    start_time = datetime.datetime.now()

    if resp.length != 0 and data.size != resp.length and type(resp.length) is int:
        logger.info(f"{data} Updating size info {data.size} -> {resp.length}")
        data.size = resp.length

    logger.info(f"{data} Download size: " + f"{resp.length} Bs, {int(resp.length / 1024)}KBs, "
                                            f"{int(resp.length / (1024 * 1024))}MBs")
    file: BufferedReader = resp.fp
    buffer_data: bytes = file.read(128*1024)
    download_data = buffer_data
    try:
        while buffer_data:
            buffer_data = file.read(128*1024)
            download_data += buffer_data
            if progress_update_callback:
                progress_update_callback(data, len(download_data), start_time)
    except Exception as e:
        logger.warning(f"{data} An error occurred: {e}")
        return None
    if len(download_data) < data.size:
        logger.warning(f"{data} Received less data than expected (recv:{len(download_data)}, expected:{data.size}). "
                       f"Stopped at {round(len(download_data) / data.size * 100)}%")
        return None
    if progress_update_callback:
        progress_update_callback(data, len(download_data), start_time)
    return download_data


# noinspection PyBroadException
def try_download(data: MatrixData, progress_update_callback=__default_progress_update_callback,
                 retry_max_attempts=DOWNLOAD_RETRY_ATTEMPTS):
    url = __get_http_download_link(data)
    logger.debug(f"{data} Starting download")

    error = None
    resp: http.client.HTTPResponse | None = None
    return_data = None
    retry_count = 0
    try:
        resp: http.client.HTTPResponse = request.urlopen(request.Request(url))
        logger.debug(f"{data} HTTP response code: {resp.getcode() }")
    except Exception as e:
        error = e
        logger.debug(f"{data} error fetching URL. {e}")
    if error is None and resp is not None:
        while return_data is None and retry_count < retry_max_attempts:
            try:
                resp = request.urlopen(request.Request(url))
                return_data = __download(resp, data, progress_update_callback=progress_update_callback)
                if return_data is None:
                    retry_count += 1
                    if retry_count < retry_max_attempts:
                        logger.info(f"{data} Retrying... {retry_count + 1}/{retry_max_attempts}")
                    else:
                        raise MatrixDataException(f"{data} Download failed too many times. Giving up.")
                else:
                    return return_data
            except Exception as e:
                retry_count += 1
                if retry_count < retry_max_attempts:
                    logger.info(f"{data} Error downloading data: {e} - Retrying... {retry_count + 1}"
                                f"/{retry_max_attempts}")
                else:
                    raise e
    else:
        logger.warning(f"{data} Unable to download using home server url {url} - {error}. Attempting to use a mirror")
        alt_server_list = __get_mirror_http_download_links(data)
        while len(alt_server_list) > 0:
            url = alt_server_list.pop()
            while return_data is None and retry_count < retry_max_attempts:
                try:
                    resp: http.client.HTTPResponse = request.urlopen(request.Request(url))
                    logger.debug(f"{data} HTTP response code: {resp.getcode()}")
                    return_data = __download(resp, data, progress_update_callback=progress_update_callback)
                except Exception as e:
                    logger.warning(f"{data} Unable to use mirror: {url} - {e}")
                    break
                if return_data is None:
                    retry_count += 1
                    if retry_count < retry_max_attempts:
                        logger.info(f"{data} Retrying... {retry_count+1}/{retry_max_attempts}")
                    else:
                        logger.warning(f"{data} Retried {retry_max_attempts} times. Giving up")
                        raise MatrixDataException("Download failed too many times")
                else:
                    return return_data
        raise MatrixDataException("Unable to find a valid url to download the content")


def __get_mirror_http_download_links(data: MatrixData) -> list[str]:
    """
    Blindly guesses other servers which may or may not have the file
    :param data:
    :return:
    """
    server_list = []
    for mxc_server in MXC_DATA_SERVER:
        if data.mxc_url.startswith(mxc_server):
            continue
        server_list.append(mxc_server)

    url_list = []
    for server in server_list:
        original_server = data.mxc_url.replace("mxc://", "").split("/")[0]
        _encrypted_filename = data.mxc_url.replace(f"mxc://{original_server}/", "")
        url_list.append(MXC_DATA_SERVER[server] + _encrypted_filename)

        server_hostname = server.replace("mxc://", "").split("/")[0]
        url_list.append(MXC_DATA_PLACEHOLDERS.format(original_server, server_hostname, _encrypted_filename))
        url_list.append(MXC_DATA_PLACEHOLDERS.format(server_hostname, original_server, _encrypted_filename))
    return url_list


def __get_http_download_link(data: MatrixData) -> str:
    for mxc_server in MXC_DATA_SERVER:
        if data.mxc_url.startswith(mxc_server):
            _encrypted_filename = data.mxc_url.replace(mxc_server, "")
            url = MXC_DATA_SERVER[mxc_server] + _encrypted_filename
            logger.info(f"{data} Download URL: {url}")
            return url

    logger.warning(f"{data} Unknown server, attempting to guess")
    server = data.mxc_url.replace("mxc://", "").split("/")[0]
    _encrypted_filename = data.mxc_url.replace(f"mxc://{server}/", "")
    url = MXC_DATA_PLACEHOLDERS.format(server, server, _encrypted_filename)

    logger.warning(f"{data} Guessed download URL (might fail): {url}")
    return url


def try_load_already_downloaded_file(data: MatrixData) -> bytes:
    file_path_ = os.path.join(os.getcwd(), data.out_file)
    if not os.path.exists(file_path_):
        encrypted_filename_ = data.mxc_url.replace("mxc://", "").split("/")[-1]
        file_path_ = os.path.join(os.getcwd(), encrypted_filename_)
        if not os.path.exists(file_path_):
            raise MatrixDataException("Unable to locate file for decrypting. "
                                      "Try renaming it to match the one in the json")
    with open(file_path_, "rb") as f_:
        return f_.read()


def sort_matrix_data_list(data_list: list[MatrixData], sort_strategy: SortStrategy,
                          chunk_size: int = DOWNLOAD_BATCH_SIZE) -> list[MatrixData]:
    if sort_strategy == SortStrategy.smallest_to_biggest.name:
        return sorted(data_list, key=lambda k: k.size)
    elif sort_strategy == SortStrategy.biggest_to_smallest.name:
        return sorted(data_list, key=lambda k: k.size, reverse=True)
    elif sort_strategy == SortStrategy.alternating.name \
            or sort_strategy == SortStrategy.chunks.name:
        data_list.sort(key=lambda k: k.size)

        def __alternate(original_list):
            _result = []
            last_element = None
            # make sure its even
            if len(original_list) % 2 != 0:
                last_element = original_list[-1]
                original_list.pop()
            # iterate through half of the original list
            for _i, element in enumerate(original_list[:len(original_list) // 2]):
                # add the element, and then the last element in reverse
                _result.append(element)
                _result.append(original_list[-(_i + 1)])
            if last_element:
                _result.insert(0, last_element)
            return _result

        if sort_strategy == SortStrategy.alternating.name:
            return __alternate(data_list)
        else:  # SortStrategy.chunks
            result = __alternate(data_list)
            for i in range(chunk_size):
                result = __alternate(result)
        logger.debug(f"sorted sizes: " + ' '.join([str(i.size) for i in result]))
        return result


def get_matrix_data_list(json_: str, ignore_invalid_children=True) -> list[MatrixData]:
    if json_ is None or not os.path.exists(json_):
        raise MatrixDataException("invalid file path")
    with open(json_, 'rb') as json_f:
        data = json.load(json_f)
    if type(data) is not list:
        if type(data) is dict and "messages" in data.keys():
            data = data["messages"]
        else:
            raise MatrixDataException("the provided json file is not a valid export of matrix events")

    data_list: list[MatrixData] = []
    repeated_filenames_count: dict[str] = {}  # used to rename files, so that they are not overwritten later
    for i, element_data in enumerate(data):
        try:
            data = get_matrix_data(None, element_data, i)
            if data.filename in repeated_filenames_count.keys():
                filename, extension = os.path.splitext(data.filename)
                data.out_file = f"{filename}_{repeated_filenames_count[data.filename]}{extension}"
                repeated_filenames_count[data.filename] += 1
            else:
                repeated_filenames_count[data.filename] = 1
            data_list.append(data)
        except MatrixDataException as e:
            logger.debug(f"Errors processing element {i}: {e}")
            if not ignore_invalid_children:
                raise e
    return data_list


def get_matrix_data(json_: str = None, array_element=None, array_index: int = -1) -> MatrixData:
    """
    load necessary data from json or the list of parsed data
    :param json_: path for json file, or json string itself
    :param array_element: create data from an already-parsed list
    :param array_index: index of item to be returned
    :return: MatrixJsonData object containing the necessary data
    """
    if array_element is not None:
        data = array_element
    else:
        if json_ is None or not os.path.exists(json_):
            raise MatrixDataException("invalid file path")
        with open(json_) as json_f:
            data = json.load(json_f)

    object_str = "json file" if json_ else f"object at index {array_index}"
    # logger.debug(f"Checking json data for index {array_index}")
    if not data:
        raise MatrixDataException(f"{object_str} is empty")

    if "content" not in data.keys():
        if "decrypted" in data.keys() and "content" in data["decrypted"].keys():
            data["content"] = data["decrypted"]
            if "content" in data["content"].keys():
                data["content"] = data["content"]["content"]
        else:
            raise MatrixDataException(f"missing 'content' key in {object_str}")

    if not set(data["content"].keys()).issuperset({"file", "body"}):
        raise MatrixDataException(f"missing 'file' or 'body' keys in {object_str}")

    if not set(data["content"]["file"].keys()).issuperset({"iv", "key", "url"}):
        raise MatrixDataException(f"missing 'iv', 'key' or 'url' keys in {object_str}")

    if "k" not in data["content"]["file"]["key"].keys():
        raise MatrixDataException(f"missing 'k' key in {object_str}")

    warns = []
    # optional
    encrypted_sha256 = b''
    if "hashes" in data["content"]["file"].keys():
        if "sha256" in data["content"]["file"]["hashes"]:
            # noinspection PyBroadException
            # appended "==" because:
            # https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
            try:
                encrypted_sha256 = base64.b64decode(data["content"]["file"]["hashes"]["sha256"] + "==")
            except Exception:
                logger.debug(f"invalid sha256 base64 at index {array_index}")
        else:
            warns.append("'sha256'")
    else:
        warns.append("'hashes'")

    # optional
    if "size" not in data["content"]["info"].keys():
        warns.append("'size'")
        size = 1
    else:
        size = data["content"]["info"]["size"]

    if warns:
        logger.warning(f"missing property {', '.join(warns)} in {object_str}")

    logger.debug(f"Extracting {object_str}")
    filename = data["content"]["body"]

    # appended "==" because:
    # https://stackoverflow.com/questions/2941995/python-ignore-incorrect-padding-error-when-base64-decoding
    iv_base64 = data["content"]["file"]["iv"] + "=="
    logger.debug(f"{filename[:8]}: Decoding IV: {iv_base64}")
    iv = base64.urlsafe_b64decode(iv_base64)

    decrypt_key_base64 = data["content"]["file"]["key"]["k"] + "=="
    logger.debug(f"Decoding decryption key: {decrypt_key_base64}")
    decrypt_key = base64.urlsafe_b64decode(decrypt_key_base64)

    return MatrixData(filename=filename,
                      iv=iv,
                      size=size,
                      mxc_url=data["content"]["file"]["url"],
                      decrypt_key=decrypt_key,
                      encrypted_sha256=encrypted_sha256,
                      index=array_index,
                      out_file=filename,
                      decrypted_sha256=b'')


def create_argument_parser() -> argparse.Namespace:
    _parser = argparse.ArgumentParser()
    _parser.description = "Processes decrypted matrix events in order to download file contents"
    group_input_file = _parser.add_mutually_exclusive_group()
    group_input_file.add_argument("-m", "--decrypted_message", type=str, metavar="FILE_PATH",
                                  help="specify the path of a json file containing a single decrypted matrix event")
    group_input_file.add_argument("-e", "--decrypted_event_list", type=str, metavar="FILE_PATH",
                                  help="specify the path of a json file containing a list of decrypted matrix events")
    group_input_file.required = True

    group_decryption = _parser.add_mutually_exclusive_group()
    group_decryption.add_argument("-f", "--decrypt_only", action="store_true",
                                  help="only decrypt the content of the downloaded file(s).")
    group_decryption.add_argument("-n", "--dont_decrypt", action="store_true",
                                  help="only download the content. [NOT IMPLEMENTED]")
    group_decryption.add_argument("-l", "--only_url_list", action="store_true",
                                  help="print the download links list, useful if you plan to download with "
                                       "a download manager")
    group_decryption.add_argument("-lu", "--only_url_list_unfiltered", action="store_true",
                                  help="print the download links list without filtering already downloaded files,"
                                       " useful if you plan to download with a download manager")
    group_decryption.add_argument("-p", "--only_url_list_plus_mirrors", action="store_true",
                                  help="only return the download link list + mirrors, useful "
                                       "if you plan to download with a download manager. [NOT IMPLEMENTED]")
    group_decryption.add_argument("-u", "--file_list", action="store_true",
                                  help="only return the filenames list, not sure how this can be useful, but whatever,"
                                       "it comes out in the same order as --only_url_list and the others btw")

    group_metadata = _parser.add_mutually_exclusive_group()
    group_metadata.add_argument("--ignore_even_if_nonexistent", action="store_true",
                                help="Skips files that were previously downloaded even if they are not in the folder. "
                                     f"Note that this only works if the file '{Metadata.METADATA_FILENAME}' exists")
    group_metadata.add_argument("--update_metadata", action="store_true",
                                help="Forces re-checking all files that were already downloaded (they must be in "
                                     "the current working directory), and creates the file "
                                     f"'{Metadata.METADATA_FILENAME}', then exits. WARNING: This operation uses A LOT "
                                     f"of RAM")

    _parser.add_argument("--delete_after_decryption", action="store_true",
                         help="Only works if --decrypt_only was specified, deletes the decrypted file after decrypting "
                              "it")
    _parser.add_argument("--mark_failed_as_done", action="store_true",
                         help="Whenever a download fails for whatever reason after the max number of attempts, add it"
                              "to the metadata list, as if it was downloaded, to prevent re-downloading it")
    _parser.add_argument("--batch_size", type=int, choices=range(1, 65),
                         metavar=f'{DOWNLOAD_BATCH_SIZE}',
                         help="Number of downloads to be done in parallel when using -e")
    _parser.add_argument("--sort", type=str, choices=[i.name for i in SortStrategy],
                         help="Sorts events based on their content size")
    _parser.add_argument("--sort_chunk_size", type=int, choices=range(1, 1024),
                         metavar=f'{DOWNLOAD_BATCH_SIZE}',
                         help="Defines the chunk size when '--sort chunk' is used, defaults to batch_size")
    _parser.add_argument("-o", "--replace", action="store_true",
                         help="Replace existing files. [NOT IMPLEMENTED]")
    _parser.add_argument("--report_interval", type=int,
                         help="Amount of seconds to wait before displaying a report of the current downloads. "
                              f"Default: {DownloadManager.REPORT_INTERVAL_SECS}.")
    _parser.add_argument("-v", "--verbose", action="store_true")

    return _parser.parse_args()


def handle_argument_parser_errors(_args: argparse.Namespace):
    if _args.decrypted_message and _args.batch_size is not None:
        print("-m and --decrypted_message cannot be used with --batch_size.")
        exit(-1)
    if _args.decrypted_message and _args.file_list:
        print("-m and --decrypted_message cannot be used with --file_list. Just open the fucking json and read the "
              "filename")
        exit(-1)
    if _args.sort_chunk_size and not _args.sort == SortStrategy.chunks.name:
        print("-sort_chunk_size should only be used with '--sort chunks'")
        exit(-1)


if __name__ == "__main__":
    args = create_argument_parser()
    handle_argument_parser_errors(args)

    if args.verbose:
        logger.setLevel(logging.DEBUG)

    if args.decrypted_message:
        __matrix_data = get_matrix_data(args.decrypted_message)
        if args.decrypt_only:
            encrypted_bytes = try_load_already_downloaded_file(__matrix_data)
            save_data(__matrix_data, encrypted_bytes)
        else:
            encrypted_bytes = try_download(__matrix_data)
            decrypted_bytes = decrypt_data(__matrix_data, encrypted_bytes)
            save_data(__matrix_data, decrypted_bytes)

    elif args.decrypted_event_list:
        if args.report_interval:
            DownloadManager.REPORT_INTERVAL_SECS = args.report_interval

        # disable logs because only urls or filenames will be printed
        if args.only_url_list or args.file_list or args.only_url_list_unfiltered:
            logger.setLevel(logging.FATAL)

        if args.batch_size:
            DOWNLOAD_BATCH_SIZE = args.batch_size

        __matrix_data = get_matrix_data_list(args.decrypted_event_list)

        if args.update_metadata:
            metadata_instance = Metadata()
            metadata_instance.apply_to_data(__matrix_data)
            metadata_instance.update_metadata(__matrix_data)
            metadata_instance.save()
            exit(0)

        if args.sort:
            __matrix_data = sort_matrix_data_list(__matrix_data, args.sort)

        # print the download links and exit
        if args.only_url_list or args.only_url_list_unfiltered:
            if not args.only_url_list_unfiltered:
                filtered = []
                metadata_instance = Metadata()
                __metadata_keys = set(metadata_instance.metadata.keys())
                __metadata_values = set(metadata_instance.get_flattened_values())
                for data_ in __matrix_data:
                    if not file_already_downloaded(data_, args.ignore_even_if_nonexistent, __metadata_keys,
                                                   __metadata_values):
                        filtered.append(data_)
                __matrix_data = filtered

            for data_ in __matrix_data:
                print(__get_http_download_link(data_))
            exit(0)

        if args.decrypt_only:
            # todo: make this multi-threaded
            for single_matrix_data in __matrix_data:
                try:
                    logger.info(f"{single_matrix_data} Reading file")
                    encrypted_bytes = try_load_already_downloaded_file(single_matrix_data)
                    if hashlib.sha256(encrypted_bytes).digest() == single_matrix_data.encrypted_sha256:
                        decrypted_bytes = decrypt_data(single_matrix_data, encrypted_bytes)
                        save_data(single_matrix_data, decrypted_bytes)
                    else:
                        logger.warning(f"{single_matrix_data} Mismatching encrypted sha256. Skipping")
                        continue
                except MatrixDataException as er:
                    logger.debug(f"{single_matrix_data} Unable to decrypt file: {er}")
            exit(0)

        # print the file name and exit
        elif args.file_list:
            for data_ in __matrix_data:
                print(data_.filename)
            exit(0)

        download_manager = DownloadManager(__matrix_data)
        download_manager.ignore_even_if_nonexistent = True if args.ignore_even_if_nonexistent else False
        download_manager.mark_failed_as_done = True if args.mark_failed_as_done else False
        download_manager.begin_batch_download()
