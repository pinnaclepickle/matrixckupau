import dataclasses
import datetime
import json
import argparse
import os


@dataclasses.dataclass
class MatrixEvent:
    data: dict

    def __hash__(self):
        return int.from_bytes(str(self.data["event_id"][1:8]).encode(), "big")


def create_argument_parser() -> argparse.Namespace:
    _parser = argparse.ArgumentParser()
    _parser.description = "Merges multiple JSON exports into a single one.\n" \
                          "Example: python3 MergeJsons.py *.json"
    _parser.add_argument("file1", type=str, nargs=1,
                         help="first json file")
    _parser.add_argument("fileN", type=str, nargs="+",
                         help="input json files")
    return _parser.parse_args()


def get_matrix_event_list(input_obj: dict, filename: str):
    if not input_obj:
        print("Not processing empty file:", filename)
    if type(input_obj) is dict and "messages" in input_obj.keys():
        event_list = input_obj["messages"]
        length = len(event_list)
        ret = []
        errors = 0
        for i, event_ in enumerate(event_list):
            print("Processing event:", i+1, "of", length, end="\r")
            if "event_id" in event_:
                ret.append(MatrixEvent(data=event_))
            else:
                errors += 1
        print("\nDone. Errors:", errors)
        return ret
    else:
        print("Error parsing file:", filename)
    return []


if __name__ == "__main__":
    args = create_argument_parser()
    files = args.file1 + args.fileN
    for file in files:
        if not os.path.exists(file):
            print("Nonexistent file:", file)
            exit(-1)

    set_ = set()
    for file in files:
        with open(file, "r") as f:
            print("Processing file:", file)
            for event in get_matrix_event_list(json.load(f), file):
                set_.add(event)

    with open(f"merged_{datetime.datetime.now().strftime('%Y-%m-%d_%H-%M-%S')}.json", "w") as f:
        json.dump([i.data for i in set_], f)
