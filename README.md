# Matrix Downloader
Downloads files from matrix without bullshit. fuck you element, if you are an Element dev, make this shit  better so that i can delete this script from existence.

## Why
Because [Element](https://element.io), the reference client for [Matrix](https://matrix.org/), fucking sucks when it comes to downloading files realibly, it either silently fails or just takes forever to say when it has failed, also you cannot cancel downloads. 

"-But why didnt people make it better for the users?" - said the innocent user 

"Why? Because fuck'em thats why."  - an Element dev or something.

## Dude, chill

No. fuck you, i spent my time making this shit because i was so fucking tired of Element, like it or not. Also this should've been here a long time ago, i was just lazy to upload it.

Enjoy

## But how tf do I use this?

You need python 3.10, maybe an earlier version works but i cannot guarantee that.

PyCrypto is not required anymore, but i recommend installing it. Might be a pain to get it to work on Windows (also if you are a Windows user just get fucked already lmao. I'm looking at you @Chibi).

Just run: `python MatrixDownloader.py -h` to see the help, there are lots of options in there, but the basic idea is:

### Downloading multiple files (from multiple events)
1. Go to an Element chat room and click on the Room's name, then Export Chat:  
   ![](./content/Screenshot1.png)
2. Select JSON. You can either get all messages since you joined the room, or just the visible portion of the chat you have loaded locally (Current Timeline). I recommend using "From the beginning" to get everything.
   - **Important:** do **NOT** select "Include Attachments", this will tell Element to download the files, YOU DON'T WANT THIS IF YOU ARE USING THIS SCRIPT, the script will do this for you, or give you the ways of achieving that more efficiently.  
   ![](./content/Screenshot2.png)
3. Wait for it to finish, you can hide the download dialog by clicking outside of it, the download will continue on the background.
4. Once finished, you now have a JSON file with the events. Congrats.
   1. Some events might have failed to decrypt, this is normal. **There is nothing I can do to help, don't bother me with this shit**, blame the Matrix architecture for that.
   2. All events are included in the JSON: messages, reactions, files, user joins, nicknames changes, etc. But MatrixDownloader will only process Files (raw files, images and videos)
5. Open a terminal and run MatrixCommander passing the JSON file path:  
    Example using Windows:
    ```
   C:\users\me\Downloads> python MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json"
   ```  
   Example using Linux:
   ```bash
   user@host:~$ python3 MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json"
   ```
   1. The downloaded files are placed in the current directory.
6. The download should begin and all files found by the script will be decrypted then saved. The number of connections can be changed (with --batch_size)  

### Downloading content from a single message (single event)
1. Go to element and get the source code of a message  
   ![](./content/Screenshot3.png)
2. Copy the JSON content  
   ![](./content/Screenshot4.png)
3. Save it in a file, for example "bunny.json"
4. Open a terminal and run MatrixCommander passing the JSON file path:  
   Example using Windows:
    ```
   C:\users\Me\Downloads> python MatrixDownloader.py -m "bunny.json"
   ```  
   Example using Linux:
    ```bash
   user@host:~$ python3 MatrixDownloader.py -m "bunny.json"
   ```  
   1. The downloaded files are placed in the current directory.

### Using an external downloader
MatrixDownloader allows you to use external downloaders such as aria2 or JDownloader2 for better control of the downloaded files. 

**Important note: The downloaded files are encrypted!!!**  

MatrixDownloader has to be used again to decrypt the files, this only works if the downloaded filename matches the encrypted file name.

For example:  
- If the `url` parameter in the JSON is this: `mxc://nerdsin.space/19c7fa73ed9f9d1e58011151c4a9266917d6ac5c`; 
- Then the filename must be `19c7fa73ed9f9d1e58011151c4a9266917d6ac5c`, otherwise MatrixDownloader won't be able to know what file to decrypt.

#### How to do it
1. Follow the steps from [Downloading multiple files](#downloading-multiple-files-from-multiple-events) up to step 4 
2. Open a terminal and run MatrixCommander passing the JSON file path **using the parameter `-l` at the end**:  
    Example using Windows:
    ```
   C:\users\me\Downloads> python MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" -l
   ```  
   Example using Linux:
   ```bash
   user@host:~$ python3 MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" -l
   ```
3. The links will be printed on the terminal, there you can copy the links, or write it to a file.  
   If you don't know how to pipe the output to a file (idk how the fuck you got here, but here is an example):  
   Example using Windows:
   ```
   C:\users\me\Downloads> python MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" -l > links.txt
   ```  
      Example using Linux:
   ```bash
   user@host:~$ python3 MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" -l > links.txt
   ```
   The download links will be in a file called "links.txt" in the same directory as the current terminal.
4. Once you have the links, open your favorite download manager (JDownloader2 for example) and paste the links there, and let it download the files. Make sure the filenames are the same as in the URL!!! See [Important note](#using-an-external-downloader)
5. Once all downloads finished, open the terminal again and **go to the folder where the encrypted files were downloaded**
6. Run MatrixDownloader again with the same JSON already used to export the downloads, but this time passing the parameter `--decrypt_only`:  
   Example using Windows:
   ```
   C:\users\me\Downloads> python MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" --decrypt_only
   ```  
      Example using Linux:
   ```bash
   user@host:~$ python3 MatrixDownloader.py -e "matrix - MyChatRoom - Chat Export - 2022-12-09T20-11-19.321Z.json" --decrypt_only
   ```
7. If everything worked, you should see an output like this:  
   ```
   2023-04-09 17:11:39 INFO: [3276]asdfile1:  Saving file: /home/user/Downloads/asdfile1.jpeg
   2023-04-09 17:11:39 INFO: [3276]asdfile1:  Saved successfully (0.21MBs)
   2023-04-09 17:11:39 INFO: [3277]asdfile2:  Reading file
   2023-04-09 17:11:39 INFO: [3277]asdfile2:  Saving file: /home/user/Downloads/asdfile2.jpg
   2023-04-09 17:11:39 INFO: [3277]asdfile2:  Saved successfully (0.11MBs)
   2023-04-09 17:11:39 INFO: [3278]asdfile3:  Reading file
   2023-04-09 17:11:39 INFO: [3278]asdfile3:  Saving file: /home/user/Downloads/asdfile3.jpg
   2023-04-09 17:11:39 INFO: [3278]asdfile3:  Saved successfully (0.1MBs)
   2023-04-09 17:11:39 INFO: [3279]asdfile4:  Reading file
   2023-04-09 17:11:39 INFO: [3279]asdfile4:  Saving file: /home/user/Downloads/asdfile4.jpg
   2023-04-09 17:11:39 INFO: [3279]asdfile4:  Saved successfully (0.04MBs)
   2023-04-09 17:11:39 INFO: [3280]asdfile5:  Reading file
   2023-04-09 17:11:39 INFO: [3280]asdfile5:  Saving file: /home/user/Downloads/asdfile5.jpg
   2023-04-09 17:11:39 INFO: [3280]asdfile5:  Saved successfully (0.02MBs)
   2023-04-09 17:11:39 INFO: [3281]asdfile6:  Reading file
   2023-04-09 17:11:39 INFO: [3281]asdfile6:  Saving file: /home/user/Downloads/asdfile6.jpg
   2023-04-09 17:11:39 INFO: [3281]asdfile6:  Saved successfully (0.03MBs)
   2023-04-09 17:11:39 INFO: [3282]0001.gif:  Reading file
   2023-04-09 17:11:39 INFO: [3282]0001.gif:  Saving file: /home/user/Downloads/0001.gif
   2023-04-09 17:11:39 INFO: [3282]0001.gif:  Saved successfully (8.95MBs)
   ```
   
# Troubleshooting

### Some files are not being downloaded despite having a valid link  
   - MatrixDownloader stores references to files that were previously downloaded, and won't download them again. This is an optimization to avoid re-downloading already downloaded files.  
   - You can bypass this optimization by ignoring already downloaded files with `--ignore_even_if_nonexistent`

### Errors about failing to use a mirror
   - This happens because MatrixDownloader has a hardcoded list of some known Matrix servers, which are used for finding the files referenced in events.  
   This is necessary because files are referenced using `mxc:/`, and there is no obvious way of figuring out which server its pointing to exactly. There might be a way, but i have no idea. If you know it, Feel free to create a Merge Request with this fix

### The code looks like shit
   - Yeah i know, my bad. I made this in a rush without expecting it to get this big, it works though.